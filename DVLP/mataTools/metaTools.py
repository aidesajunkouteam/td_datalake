# - * - coding: utf - 8 -
#
# chargement vers la refined
# usage
# metaTools.py  -d "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\TD_DATALAKE\DATALAKE"

import csv
import getopt
import logging
import os
import sys
import mysql.connector
import time
import dateutil.parser
from datetime import datetime
import sqlite3
import re

# Properties

pathDataLake = "../../DATALAKE/"
pathMetadata = pathDataLake + "METADATA"

metaDir="METADATA"
metaFilesDict={"tec": "metaDataTech.csv",
           "cur":"metaDataCurated.csv",
           "ref":"metaDataRefined.csv"}

metaDbDict={"tec": "metaDataTech.db",
           "cur":"metaDataCurated.db",
           "ref":"metaDataRefined.db"}


metaHearder={ "tec" : ["id","insertDate","hash","basename","origine","websource","landing"],
              "cur" :  ["id","processDate","websource","entrepriseId","type","domaine","page","db"],
              "ref" : ['id', 'processDate', 'websource', 'entrepriseId', 'typefile', 'domaine', 'page', 'iddb', 'base',
                     'serveur', 'tablename', 'typedata']}


SCRIPT_NAME = os.path.basename(sys.argv[0])

# --------------------------------------
# Definition logger application
# --------------------------------------
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)

logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s : [ %(levelname)s ] %(message)s",
                    datefmt='%m/%d/%Y %H:%M:%S',
                    handlers=[logging.FileHandler("Curated2Refined.log", mode='w'),
                              stream_handler])

logger = logging.getLogger(SCRIPT_NAME)


# from logging.config import fileConfig
#
# fileConfig('logging_config.ini')
# logger = logging.getLogger()


# --------------------------------------
# definition des methodes et fonctions
# --------------------------------------
def usage(exitCode):
    """
    afficher le parametre pris en compte par l'application
    :param exitCode: code de sortie après le usage
    """
    logger.debug("usage : {} ")

    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print('      ex: {} [-h] -d "path" '.format(
        SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")
    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n"
          "               /!\\ réinitialisation des chemins par défaut pour les landing et sourceweb")
    print("*" * 80)
    sys.exit(exitCode)


#################
def setMetadata(path):
    """
    initialisation de metapath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathMetadata
    logger.debug("Definition chemin meta")
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathMetadata = path
    else:
        logger.info("Creation du dossier de meta [{}] ".format(path))
        try:
            os.makedirs(path)
            pathMetadata = path
        except:
            raise IOError("Impossible de construire {}".format(path))




##################
def setDatalake(path):
    """
         initialisation du path datalake
         :param value: chemin du datalake
         :raise ValueError si path invalide
    """
    global pathDataLake

    logger.debug("Definition chemin datalake [ {} ]".format(path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathDataLake = path
        setMetadata(pathDataLake + "\\METADATA")
    else:
        raise ValueError("Chemin datalake des datas non valide : {}".format(path))


def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param appli: nom de l'application
    :return:
    """
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name, argv))
    try:
        opts, args = getopt.getopt(argv, "hd:",
                                   ["help", "datalake="])
    except getopt.GetoptError:
        usage(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-d", "--datalake"):
            logger.debug(arg)
            setDatalake(arg)
        else:
            usage(1)

########################################################################################################################
# --------------------------------------
# programme Principal
# --------------------------------------

def getSqliteConnexion(nom):
    return sqlite3.Connection(nom)


def loadMetaRefined():
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))

    sqLiteConn = getSqliteConnexion(metaDbDict["ref"])
    cursor = sqLiteConn.cursor()

    cursor.execute("DROP TABLE IF EXISTS refined;")

    sep = ","
    cols = sep.join(metaHearder["ref"])
    cols = cols.replace(',', ' text,')
    cols = cols + " text"

    sql = "CREATE TABLE IF NOT EXISTS refined ( {} text );".format(cols)
    logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
    cursor.execute(sql)

    metaRefinedFile = os.path.join(pathMetadata, metaFilesDict["ref"])

    if os.path.exists(metaRefinedFile):
        with open(metaRefinedFile, 'r+', newline='') as csvFile:
            logger.info("{} : Import Refined en SQLITE ".format(sys._getframe().f_code.co_name))
            reader = csv.DictReader(csvFile)
            cols = ",".join(metaHearder["ref"])
            sql = "INSERT INTO refined VALUES (?,?,?,?,?,?,?,?,?,?,?,?);".format(cols)
            logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
            for row in reader:
                cursor.execute(sql, list(row.values()))

    cursor.execute("SELECT COUNT(*) FROM refined")

    res = cursor.fetchone()

    logger.debug("Taille refined {}".format(res[0]))

    sqLiteConn.commit()
    sqLiteConn.close()


def isIdItemInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))
    sqLiteConn = getSqliteConnexion(metaDbDict["ref"])

    cursor = sqLiteConn.cursor()
    sql = "SELECT COUNT(*) from refined where iddb='{}';".format(id)

    cursor.execute(sql)
    res = cursor.fetchone()[0]
    logger.debug("{} RES SQLITE {} ".format(sys._getframe().f_code.co_name, res == 1))
    sqLiteConn.commit()
    sqLiteConn.close()
    return res == 1


def isEnterpriseInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))

    sqliteExport("SELECT * from refined where iddb='{}'  and tablename='dim_entreprise';".format(id))

    sqLiteConn = getSqliteConnexion(metaDbDict["ref"])
    cursor = sqLiteConn.cursor()

    sql = "SELECT COUNT(*) from refined where iddb='{}' and tablename='dim_entreprise';".format(id)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchone()[0]
    logger.debug("{} RES SQLITE {} ".format(sys._getframe().f_code.co_name, res >= 1))
    sqLiteConn.commit()
    sqLiteConn.close()
    return res >= 1


def sqliteExport(sql="SELECT * from refined"):
    """ pas propre mais c est pour debug le programme """
    sqLiteConn = getSqliteConnexion(metaDbDict["ref"])
    cursor = sqLiteConn.cursor()

    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, sql))

    cursor.execute(sql)

    with open('output.csv', 'w', newline='') as out_csv_file:
        csv_out = csv.writer(out_csv_file, quoting=csv.QUOTE_ALL)
        # write header
        csv_out.writerow([d[0] for d in cursor.description])
        # write data
        for result in cursor:
            csv_out.writerow(result)

    sqLiteConn.commit()
    sqLiteConn.close()


def isDbCuratedInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))

    sqLiteConn = getSqliteConnexion(metaDbDict["ref"])
    cursor = sqLiteConn.cursor()
    sql = "SELECT COUNT(*) from refined where id='{}'".format(id)

    cursor.execute(sql)
    res = cursor.fetchone()[0]

    logger.debug("{} RES SQLITE {} ".format(sys._getframe().f_code.co_name, res >= 1))
    sqLiteConn.commit()
    sqLiteConn.close()

    return res >= 1
########################################################################################################################
def loadMetaTech():

    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))

    sqLiteConn = getSqliteConnexion(metaDbDict["tec"])
    cursor = sqLiteConn.cursor()

    cursor.execute("DROP TABLE IF EXISTS refined;")

    sep = ","
    cols = sep.join(metaHearder["ref"])
    cols = cols.replace(',', ' text,')
    cols = cols + " text"

    sql = "CREATE TABLE IF NOT EXISTS technique ( {} text );".format(cols)
    logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
    cursor.execute(sql)

    sql = "CREATE TABLE IF NOT EXISTS technique ( {} text );".format(cols)
    logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
    cursor.execute(sql)
    logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
    cursor.execute(sql)

    metaTechFile = os.path.join(pathMetadata, metaFilesDict["tec"])

    if os.path.exists(metaTechFile):
        with open(metaTechFile, 'r+', newline='') as csvFile:
            logger.info("{} : Import Refined en SQLITE ".format(sys._getframe().f_code.co_name))
            reader = csv.DictReader(csvFile)
            cols = ",".join(metaHearder["tec"])
            sql = "INSERT INTO refined VALUES (?,?,?,?,?,?,?,?,?,?,?,?);".format(cols)
            logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
            for row in reader:
                cursor.execute(sql, list(row.values()))

    cursor.execute("SELECT COUNT(*) FROM refined")

    res = cursor.fetchone()

    logger.debug("Taille refined {}".format(res[0]))

    sqLiteConn.commit()
    sqLiteConn.close()

########################################################################################################################
def foundFile():
    key = str(input("Cle du fichier (UUID)  : "))
    loadMetaTech()
    sqLiteConn = getSqliteConnexion(metaDbDict["tec"])
    cursor = sqLiteConn.cursor()
    sql = "SELECT {} FROM technique where {}={}".format(metaHearder["tec"][""],)
    cursor.execute("SELECT * from ")
    

def traitementMenu(indice=0):
    menu = {
        0: Quit,
        1: foundFile,
        2: openMetaFile

    }
    func = menu.get(indice, lambda: 'Invalid')

    return func()


def traitement():
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))

    while (True):
        menu = {
            0: 'Quit',
            1: "Chercher Fichier dans le datalake",
            2: 'Consulter une fichier méta',
        }
        # affichage menu
        print("*" * 80)
        print("datalake : {}".format(pathDataLake))
        print("*" * 80)
        for key, label in menu.items():
            print("{} : {}".format(key, label))

        choice = int(input("Choix : "))
        traitementMenu(choice)


logger.error("Erreur dans le traitement ! ARRET DU PROGRAMME ." + sys.exc_info()[0])




if __name__ == "__main__":
    print("*" * 80)
    print("PROGRAMME SOURCE WEB TO LANDING")
    print("*" * 80)
    print()

    # Recupération des arguments
    parseArg(sys.argv[1:])
    if pathDataLake == None:
        usage(1)

    traitement()
