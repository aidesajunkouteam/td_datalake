# - * - coding: utf - 8 -
#
# Recupération des données depuis la sourceWeb
# passage en Landing
# construction meta technique
# fromWeb2Landing.py -d "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\TD_DATALAKE\DATALAKE" -t 30 -f %FILTERWEB%

########################################################################################################################
# Chargement des Lib
import csv

import fnmatch
import getopt
import hashlib
import logging
import os
import sys
import time
import uuid
from datetime import datetime
import pandas as pd

########################################################################################################################
# Properties
pathDataLake = "../../DATALAKE/"
pathSource = pathDataLake + "0_SOURCE_WEB"
pathLanding = pathDataLake + "1_LANDING_ZONE"
pathMetadata = pathDataLake + "METADATA"
metaDataTechName = "metaDataTech.csv"
pathFileError = pathDataLake + "ERRORFILE"
fileFilter = '*.html'
timer = 30
SCRIPT_NAME = os.path.basename(sys.argv[0])
########################################################################################################################


logger = None

loggingLevel = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

# --------------------------------------
# definition des methodes et fonctions
# --------------------------------------
def usage(exitCode):
    """
    afficher le parametre pris en compte par l'application
    :param exitCode: code de sortie après le usage
    """
    # logger.debug("usage : {} ")

    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print('      template: {} [-h] [-f "filter"] [-d "path"] [-t seconde] [-l LVL]'.format(SCRIPT_NAME))
    print('            ex: {} -f "*.html" -d "chemin" --timer 10'.format(SCRIPT_NAME))
    print('            ex: {} -f "*GLASSDOOR*" -d "chemin" --timer 10'.format(SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")
    print(" -f | --filter : \n"
          "              filtrage sur les fichiers du dossier source. \n"
          "              *      reconnait n'importe quoi. \n"
          "              ?      reconnait n'importe quel caractère\n"
          "              [seq]  reconnait n'importe quel caractère dans seq\n"
          "              [!seq] reconnait n'importe quel caractère qui n'est pas dans dans seq")
    print(" -t | --timer : \n"
          "              cadence de scanning.")
    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n"
          "               /!\\ réinitialisation des chemins par défaut pour les landing et sourceweb")
    print(" -l | --logger : \n"
          "              niveau de log {}. ".format(loggingLevel)
          )
    print("*" * 80)
    sys.exit(exitCode)


#################
def setSource(path):
    """
    initialisation de webSourcePath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))

    global pathSource
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathSource = path
    else:
        raise ValueError("Chemin d'origine web des datas non valide : {}".format(path))


##################
def setLanding(path):
    """
    initialisation de LandingPath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathLanding
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathLanding = path
    else:
        logger.info("Creation du dossier de landing [{}] ".format(path))
        try:
            os.makedirs(path)
            pathLanding = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setTimer(value):
    """
      initialisation du timer de scan
      :param value: valeur du timer
      :raise ValueError si timer  inférieur ou égal à 0
      """
    global timer

    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, timer))
    if int(value) >= 0:
        timer = int(value)
    else:
        raise ValueError("Valeur pour le timer invalide ")


#################
def setMetadata(path):
    """
    initialisation de metapath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathMetadata
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathMetadata = path
    else:
        logger.info("Creation du dossier de meta [{}] ".format(path))
        try:
            os.makedirs(path)
            pathMetadata = path
        except:
            raise IOError("Impossible de construire {}".format(path))


#################
def setErrorFile(path):
    """
    initialisation de errorFile
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathFileError
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathFileError = path
    else:
        logger.info("Creation du dossier de errorFile [{}] ".format(path))
        try:
            os.makedirs(path)
            pathFileError = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setLogger(datalakePath, lvl):
    """
       initialisation de dossierLogFile
       :param path: valeur de path
       :raise ValueError si chemin non valide
       """
    global logger

    if os.path.exists(datalakePath) and (not os.path.isfile(datalakePath)):
        pathlog = os.path.join(datalakePath, "LOG")

        # logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
        if os.path.exists(pathlog) and (not os.path.isfile(pathlog)):
            pathlog = pathlog
        else:
            print("Creation du dossier de log [{}] ".format(pathlog))
            try:
                os.makedirs(pathlog)
                pathlog = pathlog
            except:
                raise IOError("Impossible de construire {}".format(pathlog))

        pathfilelog = os.path.join(pathlog, "LOG_" + SCRIPT_NAME.upper() + ".log")
        if not lvl in loggingLevel:
            lvl = 'INFO'
        else:
            lvl = lvl.upper()

        print("Niveau Logger " + lvl)
        print("Fichier logger " + pathfilelog)

        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(lvl)

        file_handler = logging.FileHandler(filename=pathfilelog,mode="w",encoding="utf-8")
        file_handler.setLevel(logging.DEBUG)

        logging.basicConfig(level=logging.DEBUG,
                            format="%(asctime)s : [ %(levelname)s ] %(message)s",
                            datefmt='%m/%d/%Y %H:%M:%S',
                            handlers=[file_handler,
                                      stream_handler])

        logger = logging.getLogger(SCRIPT_NAME)
        logger.info("LOGGER ACTIF....")

        print("Activation du logger ")
    else:
        print("Impossible de construire le chemin du fichier log sur {}".format(datalakePath))
        raise IOError("Impossible de construire le chemin du fichier sur log {}".format(datalakePath))


def setDatalake(path):
    """
         initialisation du path datalake
         :param value: chemin du datalake
         :raise ValueError si path invalide
    """
    global pathDataLake

    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathDataLake = path
        setSource(pathDataLake + "\\0_SOURCE_WEB")
        setLanding(pathDataLake + "\\1_LANDING_ZONE")
        setMetadata(pathDataLake + "\\METADATA")
        setErrorFile(pathDataLake + "\\ERRORFILE")

        # logging.basicConfig(filename=(pathDataLake + "\\LOG\\WebToLanding.log"))
    else:
        raise ValueError("Chemin datalake des datas non valide : {}".format(path))


##################
def setFilter(newFilter):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, newFilter))
    global fileFilter
    oldfilter = fileFilter
    if newFilter == '':
        fileFilter = '*.html'
    else:
        fileFilter = newFilter

    logger.info("Passage du filtre de {} à {}".format(oldfilter, fileFilter))


def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param lvl: nom de l'application
    :return:
    """
    # logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, argv))
    try:
        opts, args = getopt.getopt(argv, "hf:d:t:l:", ["help", "filter=", "datalake=", "timer=", "logger="])
    except getopt.GetoptError:
        usage(2)

    filter = fileFilter
    path = pathDataLake
    pause = timer
    lvlLog = 'DEBUG'

    for opt, arg in opts:
        #        logger.debug("traitement de {} ".format(opt))
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-f", "--filter"):
            filter = arg

        elif opt in ("-t", "--timer"):
            pause = arg

        elif opt in ("-d", "--datalake"):
            path = arg
        elif opt in ("-l", "--logger"):
            lvlLog = arg
        else:
            usage(1)

    return path, pause, filter, lvlLog


################
def startScanning():
    """
    Scanning du dossier web
    :return:
    """
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    files = fnmatch.filter(os.listdir(pathSource), fileFilter)
    logger.info("Il y a {} à traiter.".format(len(files)))

    for file in files:
        logger.info("Traitement de {} ".format(file))
        origine = pathSource + "\\" + file
        try:
            meta = createMeta(origine)
            moveToLanding(meta)
            saveMeta(meta)
        except:
            moveToError(origine)
            logger.warning(sys.exc_info()[0])


################
def getHash(file):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, file))
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(file, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    hash = hasher.hexdigest();
    return hash


###############
def inDatalake(meta):
    """
    Verification si pas déja pris en compte.
    :param meta: Meta qui vont permettre de vérifier.
    :return: True si déjà pris en compte sinon False
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, meta))
    metaFilePath = pathMetadata + "\\" + metaDataTechName

    if os.path.exists(metaFilePath):
        df = pd.read_csv(metaFilePath, sep=",", usecols=meta.keys())
        inMeta = df[df["hash"] == meta["hash"]].shape[0] > 0
        logger.debug("HASH [{}] in META {} ".format(meta["hash"], inMeta))
        return inMeta
    else:
        return False


################
def createMeta(file):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, os.path.basename(file)))
    # RAPPEL
    # GLASSDOOR : 13546-AVIS-SOC-GLASSDOOR-E12966_P1.html => [KEY]-[TYPE]-[DOMAINE]-[WEBSOURCE]-[ENTERPRISEKEY_P[NUMPAGE]]
    # LINKEDIN : 13546-INFO-EMP-LINKEDIN-FR-1599984246.html => [KEY]-[TYPE]-[DOMAINE]-[WEBSOURCE]-[ENTERPRISEKEY]
    basename = os.path.basename(file)
    parts = basename.split("-")

    id = str(uuid.uuid4())
    hash = getHash(file)

    meta = {"id": id,
            "insertDate": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "hash": hash,
            "basename": basename,
            "origine": file,
            "websource": parts[3],
            "landing": pathLanding + "\\" + parts[3] + "\\" + id
            }
    logger.debug("META {} ".format(meta))

    if inDatalake(meta):
        raise ValueError("Data {} déjà prise en compte".format(basename))

    return meta


#################
def saveMeta(meta):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, meta["id"]))

    if not os.path.isdir(pathMetadata):
        logger.info("Creation du dossier meta {}".format(pathMetadata))
        os.makedirs(pathMetadata)

    metaFilePath = os.path.join(pathMetadata ,"metaDataTech.csv")
    csv_columns = meta.keys()
    writeHeader = not os.path.isfile(metaFilePath)
    logger.debug("{} writeHeader [{}]".format(sys._getframe().f_code.co_name, writeHeader))
    try:
        with open(metaFilePath, 'a+', newline='', encoding="utf-8") as metafile:
            writer = csv.DictWriter(metafile, fieldnames=csv_columns, quoting=csv.QUOTE_ALL)
            if writeHeader:
                writer.writeheader()
            writer.writerow(meta)

        logger.info("{} Meta sauvé dans {} ".format(sys._getframe().f_code.co_name, (metaFilePath)))

    except TypeError as err:
        logger.error("Impossible d'ecrire dans le fichier METATECH : {}".format(err))

    logger.debug("OUT {} args [{}]".format(sys._getframe().f_code.co_name, meta["id"]))

#################
def moveToLanding(meta):
    logger.debug("ENTER IN {} args [{} -> {}]".format(sys._getframe().f_code.co_name, meta["origine"], meta["landing"]))

    try:
        dir = os.path.dirname(meta["landing"])
        if not os.path.exists(dir):
            os.makedirs(dir)
        os.rename(meta["origine"], meta["landing"])
        logger.info("{} en zone Landing.".format(meta["landing"]))
    except:
        raise IOError("Impossible de déplacer {}".format(meta["origine"]))


################
def moveToError(origine):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, origine))
    errorFile = pathFileError + "\\" + str(datetime.now().timestamp()) + "_" + os.path.basename(origine)
    try:
        os.rename(origine, errorFile)
    except:
        logger.error("Impossible de déplacer le fichier dans {}".format(errorFile))


#################
def traitement():
    """
    boucle de Traitement principal
    :return:
    """
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))
    while not os.path.isfile("web.stop"):
        startScanning()
        t = 0
        logger.info("Prochain scan dans {} seconde(s)".format(timer - t))
        while t < timer:
            t = t + 1
            logger.debug("Prochain scan dans {} seconde(s)".format(timer - t))
            time.sleep(1)


# --------------------------------------
# programme Principal
# --------------------------------------

if __name__ == "__main__":
    print("*" * 80)
    print("PROGRAMME SOURCE WEB TO LANDING")
    print("*" * 80)
    # Recupération des arguments
    path, pause, filter, lvlLog = parseArg(sys.argv[1:])

    setLogger(path, lvlLog)

    setDatalake(path)
    setTimer(pause)
    setFilter(filter)

    traitement()
