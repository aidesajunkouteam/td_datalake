# - * - coding: utf - 8 -
#
# Recupération des données depuis la sourceWeb
# passage en Landing
# construction meta technique

# fromLanding2Curated.py -d "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\TD_DATALAKE\DATALAKE" -t 30

# Chargement des Lib
import csv
import getopt
import sys
import logging
import os
import time
import re
from bs4 import BeautifulSoup
import lxml
import pandas as pd
from datetime import datetime
import json

# Properties
pathDataLake = "../../DATALAKE/"
pathCurated = pathDataLake + "2_CURATED_ZONE"
curatedNameSuffix = "_curatedDb.csv"

pathLanding = pathDataLake + "1_LANDING_ZONE"
pathMetadata = pathDataLake + "METADATA"
metaDataTechName = "metaDataTech.csv"
metaDataCurName = "metaDataCurated.csv"
pathFileError = pathDataLake + "ERRORFILE"
socAvisDbField = ["id", "websource", "entrepriseId", "entreprise", "page", "mean", "avisid", "avisdate", "avisnote",
                  "avisauteur", "avisrecommandations", "commentaires"]
socInfoDbField = ["id", "websource", "entrepriseId", "entreprise", "page", "ville", "Site Web", "Siège social",
                  "Taille", "Fondé en", "Type", "Secteur", "Revenu"]
EmpDbField = ['id', 'websource', 'type', 'domaine', 'page', 'emploisId', 'titreEmplois', 'localisation', 'entreprise',
              'typeEmplois', 'fonctions', 'secteurs']

timer = 30

SCRIPT_NAME = os.path.basename(sys.argv[0])

# --------------------------------------
# Definition logger application
# --------------------------------------


logger = None

loggingLevel = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

# stream_handler = logging.StreamHandler()
# stream_handler.setLevel(logging.INFO)
#
# logging.basicConfig(level=logging.DEBUG,
#                     format="%(asctime)s : [ %(levelname)s ] %(message)s",
#                     datefmt='%m/%d/%Y %H:%M:%S',
#                     handlers=[logging.FileHandler("Landing2Curated.log", mode='w', encoding="utf-8"),
#                               stream_handler])
#
# logger = logging.getLogger(SCRIPT_NAME)


# --------------------------------------
# definition des methodes et fonctions
# --------------------------------------
def usage(exitCode):
    """
    afficher le parametre pris en compte par l'application
    :param exitCode: code de sortie après le usage
    """
    logger.debug("usage : {} ")

    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print('      ex: {} [-h] [-d "path"] [-t seconde] [-l lvl]'.format(SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")
    print(" -t | --timer : \n"
          "              cadence de scanning.")
    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n"
          "               /!\\ réinitialisation des chemins par défaut pour les landing et sourceweb")
    print(" -l | --logger : \n"
          "              niveau de log {}. ".format(loggingLevel)
          )
    print("*" * 80)
    sys.exit(exitCode)


#################
def setCurated(path):
    """
    initialisation de webSourcePath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, path))
    global pathCurated
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathCurated = path
    else:
        logger.info("Creation du dossier de curatedZone [{}] ".format(path))
        try:
            os.makedirs(path)
            pathCurated = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setLanding(path):
    """
    initialisation de LandingPath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathLanding
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathLanding = path
    else:
        logger.info("Creation du dossier de landing [{}] ".format(path))
        try:
            os.makedirs(path)
            pathLanding = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setTimer(value):
    """
      initialisation du timer de scan
      :param value: valeur du timer
      :raise ValueError si timer  inférieur ou égal à 0
      """
    global timer

    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, value))
    if int(value) >= 0:
        timer = int(value)
    else:
        raise ValueError("Valeur pour le timer invalide ")


#################
def setMetadata(path):
    """
    initialisation de metapath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathMetadata
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathMetadata = path
    else:
        logger.info("Creation du dossier de meta [{}] ".format(path))
        try:
            os.makedirs(path)
            pathMetadata = path
        except:
            raise IOError("Impossible de construire {}".format(path))


#################
def setErrorFile(path):
    """
    initialisation de errorFile
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathFileError
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathFileError = path
    else:
        logger.info("Creation du dossier de errorFile [{}] ".format(path))
        try:
            os.makedirs(path)
            pathFileError = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setDatalake(path):
    """
         initialisation du path datalake
         :param value: chemin du datalake
         :raise ValueError si path invalide
    """
    global pathDataLake

    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathDataLake = path
        setCurated(pathDataLake + "\\2_CURATED_ZONE")
        setLanding(pathDataLake + "\\1_LANDING_ZONE")
        setMetadata(pathDataLake + "\\METADATA")
        setErrorFile(pathDataLake + "\\ERRORFILE")
    else:
        raise ValueError("Chemin datalake des datas non valide : {}".format(path))

##########################################
def setLogger(datalakePath, lvl):
    """
       initialisation de dossierLogFile
       :param path: valeur de path
       :raise ValueError si chemin non valide
       """
    global logger

    if os.path.exists(datalakePath) and (not os.path.isfile(datalakePath)):
        pathlog = os.path.join(datalakePath, "LOG")

        # logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
        if os.path.exists(pathlog) and (not os.path.isfile(pathlog)):
            pathlog = pathlog
        else:
            print("Creation du dossier de log [{}] ".format(pathlog))
            try:
                os.makedirs(pathlog)
                pathlog = pathlog
            except:
                raise IOError("Impossible de construire {}".format(pathlog))

        pathfilelog = os.path.join(pathlog, "LOG_" + SCRIPT_NAME.upper() + ".log")
        if not lvl in loggingLevel:
            lvl = 'INFO'
        else:
            lvl = lvl.upper()

        print("Niveau Logger " + lvl)
        print("Fichier logger " + pathfilelog)

        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(lvl)

        file_handler = logging.FileHandler(filename=pathfilelog,mode="w",encoding="utf-8")
        file_handler.setLevel(logging.DEBUG)

        logging.basicConfig(level=logging.DEBUG,
                            format="%(asctime)s : [ %(levelname)s ] %(message)s",
                            datefmt='%m/%d/%Y %H:%M:%S',
                            handlers=[file_handler,
                                      stream_handler])

        logger = logging.getLogger(SCRIPT_NAME)
        logger.info("LOGGER ACTIF....")

        print("Activation du logger ")
    else:
        print("Impossible de construire le chemin du fichier log sur {}".format(datalakePath))
        raise IOError("Impossible de construire le chemin du fichier sur log {}".format(datalakePath))


##################
def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param appli: nom de l'application
    :return:
    """
    try:
        opts, args = getopt.getopt(argv, "hd:t:l:", ["help", "datalake=", "timer=","logger="])
    except getopt.GetoptError:
        usage(2)


    path = pathDataLake
    pause = timer
    lvlLog = 'DEBUG'

    for opt, arg in opts:
        #        logger.debug("traitement de {} ".format(opt))
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-t", "--timer"):
            pause = arg
        elif opt in ("-d", "--datalake"):
            path = arg
        elif opt in ("-l", "--logger"):
            lvlLog = arg
        else:
            usage(1)

    return path, pause, lvlLog


#################
def loadKeyInCurated():
    """
    Chargement des data deja pris en compte dans la curated zone
    :return: list des key(id) deja en currated
    """
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    inCurated = []
    metaCuratedFile = pathMetadata + "\\" + metaDataCurName
    if os.path.exists(metaCuratedFile):
        df = pd.read_csv(metaCuratedFile, sep=",")
        inCurated = df["id"].tolist()

    return inCurated


def getLisToProcess(inCurrated):
    """
    recherche la list des fichiers à traiter dans le landing à partir du des metaLanding
    :param inCurrated:
    :return: liste des cles à process
    """
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, inCurrated))
    metaLandingFile = pathMetadata + "\\" + metaDataTechName
    landingList = {}
    if os.path.exists(metaLandingFile):
        df = pd.read_csv(metaLandingFile, sep=",")
        df = df.loc[- df["id"].isin(list(set(inCurrated)))]
        landingList = df.T.to_dict().values()

    logger.debug("liste en landing {} .".format(landingList))

    return landingList


def extractMetaGlassdoor(metaTech):
    """
    Extraction des meta curated a partir de glassdoor
    :param metaTech:
    :return: metacurated
    """
    # RAPPEL
    # GLASSDOOR : 13546-AVIS-SOC-GLASSDOOR-E12966_P1.html => [KEY]-[TYPE]-[DOMAINE]-[WEBSOURCE]-[ENTERPRISEKEY_P[NUMPAGE]].html
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, metaTech))
    parts = metaTech['basename'].split(".")[0].split("-")

    meta = {"id": metaTech["id"],
            "processDate": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "websource": parts[3],
            "entrepriseId": parts[4].split("_")[0],
            "type": parts[1],
            "domaine": parts[2],
            "page": parts[4].split("_")[1].replace("P", "")
            }
    return meta

    pass


def extractMetaLinkedin(metaTech):
    """
    Extraction meta a aprtir de LinkedIn
    :param metaTech:
    :return: metaCurated
    """
    # RAPPEL
    # LINKEDIN : 13546-INFO-EMP-LINKEDIN-FR-1599984246.html => [KEY]-[TYPE]-[DOMAINE]-[WEBSOURCE]-[NAT]-[EMPLOIKEY].html
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, metaTech))
    parts = metaTech['basename'].split(".")[0].split("-")

    meta = {"id": metaTech["id"],
            "processDate": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "websource": parts[3],
            "entrepriseId": parts[5], ## OK c est pas l'entreprise mais ca va faire office
            "type": parts[1],
            "domaine": parts[2],
            "page": 1
            }
    return meta
    pass


def extractMeta(metaTech):
    """
    factory pour extraction des metaCurated selon le websource
    :param metaTech:
    :return: metaCurated
    """
    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, metaTech))
    logger.debug("Creation des Meta pour {}".format(metaTech["id"]))
    if metaTech["websource"] == 'GLASSDOOR':
        return extractMetaGlassdoor(metaTech)
    elif metaTech["websource"] == 'LINKEDIN':
        return extractMetaLinkedin(metaTech)
    else:
        logger.warning("Type de websource inconnu {}. Faire évoluer le produit.".format(metaTech["websource"]))


def getSoupFromFile(filePath):
    """
    Recuperation des données html en soup
    :param filePath:
    :return: soupData
    """

    logger.debug("ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, filePath))
    try:
        with open(filePath, "r", encoding="utf-8") as f:
            myHTMLContents = f.read()
            soup = BeautifulSoup(myHTMLContents, 'lxml')
        return soup
    except:
        logger.warning("Impossible de traiter {} cause {} ".format(filePath, sys.exc_info()[0]))
        raise


def glassdoorAviExtractEntrepiseName(mySoup):
    myTest = mySoup.find_all('div', attrs={"class": "header cell info"})[0].span.contents[0]
    if (myTest == []):
        result = 'NULL'
    else:
        result = myTest
    return result


def glassdoorAviExtractAvis(Soup):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    myTest = Soup.find_all('li', attrs={'class': 'empReview'})

    if (myTest == []):
        result = 'NULL'
    else:
        reviews = []
        for review in myTest:

            # Formatage de l'id d'avis
            avisID = review['id'].split('_')[1]
            # Recuperation des recommandations
            recommandations = []
            recommends = review.find_all('div', attrs={'class': 'col-sm-4'})
            if (recommends == []):
                recommandations = 'NULL'
            else:
                for recommend in recommends:
                    recommandations.append(recommend.span.text)

            # Recuperation des commentaires
            commentaires = {}
            comments = review.find_all('div', attrs={'class': 'v2__EIReviewDetailsV2__fullWidth'})
            if (comments == []):
                comments = review.find_all('div', attrs={'class': 'mt-md'})
                if (comments == []):
                    comment = 'NULL'
                else:
                    for comment in comments:
                        CT = comment.find_all('p')
                        commentaires[CT[0].text] = CT[1].text
                        # commentaires[CT[0].text] = CT[1].text.decode('utf-8','ignore').encode("utf-8")
            else:
                comment = []
                for comment in comments:
                    AvisTitle = comment.find('p', attrs={'class': 'mb-0'})
                    AvisText = comment.find('p', attrs={'class': 'mt-0'})
                    if AvisTitle != None:
                        commentaires[AvisTitle.text] = AvisText.text
                        # commentaires[AvisTitle.text] = AvisText.text.decode('utf-8','ignore').encode("utf-8")

            # Recupération et normalisation de la date
            AvisDate = review.find('time')
            if AvisDate != None:
                AvisDateTxt = AvisDate.text
                AvisDateDT = datetime.strptime(AvisDateTxt, '%b %d, %Y')
                AvisDateTxt = datetime.strftime(AvisDateDT, "%d/%m/%Y %H:%M:%S")

            # Récupération notes
            AvisNote = review.find('span',
                                   attrs={'class': 'gdStars gdRatings sm mr-sm mr-md-std stars__StarsStyles__gdStars'})
            note = None

            if AvisNote != None:
                note = AvisNote.find('span', attrs={'class': 'value-title'}).get('title')

            # structuration des données d'avis
            currentAvis = {
                "avisid": avisID,
                "avisdate": AvisDateTxt,
                "avisnote": note,
                "avisauteur": review.find('span', attrs={'class': 'authorInfo'}).text,
                "avisrecommandations": recommandations,
                "commentaires": commentaires
            }
            reviews.append(currentAvis)

        result = reviews

    return result


def glassdoorAvisExtractScoreMoyen(mySoup):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    myTest = mySoup.find_all('div', attrs={
        'class': 'v2__EIReviewsRatingsStylesV2__ratingNum v2__EIReviewsRatingsStylesV2__large'})[0].contents[0]
    if (myTest == []):
        result = 'NULL'
    else:
        result = myTest
    return result


def extractDataAvisGlassdoor(metaCurated, metaTech):
    """
    Extrcation des données de Type AVIS sur une société Sur Glassdoor
    :param metaCurated:
    :param metaTech:
    :return: data extraite
    """
    logger.debug(
        "ENTER IN {} arg [{} / {}]".format(sys._getframe().f_code.co_name, metaTech["id"], metaCurated["entrepriseId"]))

    try:
        soupData = getSoupFromFile(metaTech["landing"])
        enterprise = glassdoorAviExtractEntrepiseName(soupData)
        meanScore = glassdoorAvisExtractScoreMoyen(soupData)
        reviews = glassdoorAviExtractAvis(soupData)
        logger.debug(reviews)
        for review in reviews:
            try:
                logger.debug(review)
                data = {"id": metaCurated["id"],
                        "websource": metaCurated["websource"],
                        "entrepriseId": metaCurated["entrepriseId"],
                        "entreprise": enterprise,
                        "page": metaCurated["page"],
                        "mean": meanScore,
                        }
                for k in review.keys():
                    data[k] = review[k]

                saveDataToCVS(metaCurated, data)

            except KeyError as err:
                logger.warning(err.args)

        return data
    except Exception as inst:
        logger.warning(inst.args)
        raise inst


def glassdoorSocExtractEntrepiseName(soupData):
    """
    Extraction du nom de l'entreprise depuis une page Type info société glassdoor
    :param soupData:
    :return: nom de l'entreprise
    """
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))
    myTest = soupData.find_all('h1')[0]

    if (myTest == []):
        raise ValueError("Impossible de touver le nom de l'entreprise dans ce fichier")
    else:
        myTxtTmp = str(myTest)
        myTxtTmp1 = re.sub(r'(.*)data-company="(.*)" title="">(.*)', r'\2', myTxtTmp).strip()
        result = myTxtTmp1

    logger.debug("OUT OFF {} - {}".format(sys._getframe().f_code.co_name, result))
    return result


def glassdoorSocExtractCity(soupData):
    """
    Extrcation de la ville a partir d'une page glassdoor info de société
    :param soupData:
    :return: nom de la ville ou NULL
    """
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))
    myTest = str(soupData.find_all('div', attrs={'class': "infoEntity"})[1].span.contents[0])

    if (myTest == []):
        raise ValueError("Impossible de touver la ville dans ce fichier")
    else:
        myTxtTmp = str(myTest)
        myTxtTmp1 = re.sub(r'(.*)<h1 class=" strong tightAll" data-company="(.*)" title="">(.*)', r'\2', myTxtTmp)
        result = myTxtTmp1
    logger.debug("OUT OFF {} - {}".format(sys._getframe().f_code.co_name, result))

    return result


def glassdoorSocDetail(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))

    datas = soupData.find('div', attrs={'class': "info flexbox row col-hh"})

    result = {}
    if (datas == []):
        raise ValueError("Impossible de touver les details pour cette entreprise")
    else:
        dtls = datas.find_all('div', attrs={'class': 'infoEntity'})
        for dtl in dtls:
            key = dtl.label.text
            value = dtl.span.text
            result[key] = value

    logger.debug("OUT OFF {} - {}".format(sys._getframe().f_code.co_name, result))

    return result


def extractDataSocGlassdoor(metaCurated, metaTech):
    """
    Extraction des données depuis SOC Glassdoor
    :param metaCurated:
    :param metaTech:
    :return:
    """
    logger.debug("ENTER IN {} arg [{} / {}]".format(sys._getframe().f_code.co_name, metaCurated, metaTech))

    try:

        soupData = getSoupFromFile(metaTech["landing"])
        enterprise = glassdoorSocExtractEntrepiseName(soupData)
        ville = glassdoorSocExtractCity(soupData)
        details = glassdoorSocDetail(soupData)

        data = {"id": metaCurated["id"],
                "websource": metaCurated["websource"],
                "entrepriseId": metaCurated["entrepriseId"],
                "entreprise": enterprise,
                "page": metaCurated["page"],
                "ville": ville
                }
        # ce qu'on veut garder de la page
        wantedKey = ['Site Web', 'Siège social', 'Taille', 'Fondé en', 'Type', 'Secteur', 'Revenu']
        for key in wantedKey:
            if key in details:
                data[key] = str(details[key]).strip()
            else:
                data[key] = 'Inconnu'
        saveDataToCVS(metaCurated, data)
    except ValueError as vr:
        logger.warning(" {} : {}".format(sys._getframe().f_code.co_name.upper(), vr.args))
        raise
    except Exception as inst:
        logger.warning(" {} : {}".format(sys._getframe().f_code.co_name.upper(), inst.args))
        raise inst


def linkedinEmpTitleExtract(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    text = soupData.find('h1', attrs={'class': 'topcard__title'})
    if (text == []):
        result = 'NULL'
    else:
        result = text.text
    return result


def linkedinEmpLocalisation(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    text = soupData.find_all('span', attrs={'class': 'topcard__flavor topcard__flavor--bullet'})
    if (text == []):
        result = 'NULL'
    else:
        myTest = str(text[0].text)
        if (myTest == []):
            result = 'NULL'
        else:
            result = myTest
    return result


def linkedinEmpEntreprise(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    text = soupData.find_all('span', attrs={'class': 'topcard__flavor'})
    if (text == []):
        result = 'NULL'
    else:
        text = str(text[0].text)
        if (text == []):
            result = 'NULL'
        else:
            result = text
    return result


def linkedinEmpCritere(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    text = soupData.find_all('li', attrs={'class': 'job-criteria__item'})
    typeEmp = None
    fonctions = {}
    secteurs = {}
    logger.debug("ENTER IN {} arg [{} / {}/ {}]".format(sys._getframe().f_code.co_name, typeEmp, fonctions, secteurs))

    if (text == []):
        return typeEmp, fonctions, secteurs
    else:
        for critere in text:
            typeCrit = critere.find('h3',
                                    attrs={'class': 'job-criteria__subheader'}).text
            if typeCrit == 'Type d’emploi':
                typeEmp = critere.find('span',
                                       attrs={'class': 'job-criteria__text job-criteria__text--criteria'}).text
            elif typeCrit == 'Fonction':
                toExtract = critere.find_all('span',
                                             attrs={'class': 'job-criteria__text job-criteria__text--criteria'})
                index=0
                for f in toExtract:
                    fonctions[str(index)] = f.text
                    index += 1
            elif typeCrit == 'Secteurs':
                toExtract = critere.find_all('span',
                                             attrs={'class': 'job-criteria__text job-criteria__text--criteria'})
                index = 0
                for f in toExtract:
                    secteurs[str(index)] = f.text


    logger.debug("ENTER IN {} arg [{} / {}/ {}]".format(sys._getframe().f_code.co_name, typeEmp, fonctions, secteurs))

    return typeEmp, fonctions, secteurs


def linkedinEmpComment(soupData):
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    text = soupData.find_all('div', attrs={'class': 'description__text description__text--rich'})
    if (text == []):
        result = 'NULL'
    else:
        text = str(text[0].text)
        if (text == []):
            result = 'NULL'
        else:
            result = text
    return result


def extractDataLinkedin(metaCurated, metaTech):
    logger.debug("ENTER IN {} arg [{} / {}]".format(sys._getframe().f_code.co_name, metaTech["id"], metaCurated["entrepriseId"]))
    try:
        soupData = getSoupFromFile(metaTech["landing"])
        # EmpDbField = ['id', 'websource', 'type', 'domaine', 'page', 'emploisId', 'titreEmplois', 'localisation','entreprise', 'typeEmplois', 'fonctions', 'secteurs']

        typeEmp, fonctions, secteurs = linkedinEmpCritere(soupData)
        logger.debug("ENTER IN {} arg [{} / {}/ {}]".format(sys._getframe().f_code.co_name, typeEmp, fonctions, secteurs))

        data = {
            'id': metaTech["id"],
            'websource': metaTech["websource"],
            'type': metaCurated["type"],
            'domaine': metaCurated["domaine"],
            'page': metaCurated["page"],
            'emploisId': metaCurated["entrepriseId"], ## Voilà c est corrigé entrepriseId devient EmploisId
            'titreEmplois': linkedinEmpTitleExtract(soupData),
            'localisation':linkedinEmpLocalisation(soupData),
            'entreprise': linkedinEmpEntreprise(soupData),
            'typeEmplois': typeEmp,
            'fonctions': json.dumps(fonctions),
            'secteurs':json.dumps(secteurs),
            'commentaire':linkedinEmpComment(soupData)
        }

        saveDataToCVS(metaCurated, data)
    except Exception as inst:
        logger.warning(inst.args)
        raise inst




def extractData(metaCurated, metaTech):
    """
    factory Extraction des données pour la curatedDb
    :param metaTech:
    :return:
    """
    logger.debug("ENTER IN {} arg [{} / {}]".format(sys._getframe().f_code.co_name, metaCurated, metaTech))

    if metaCurated["websource"] == 'GLASSDOOR' and metaCurated["type"] == 'AVIS':
        return extractDataAvisGlassdoor(metaCurated, metaTech)
    elif metaCurated["websource"] == 'GLASSDOOR' and metaCurated["type"] == 'INFO':
        return extractDataSocGlassdoor(metaCurated, metaTech)
    elif metaCurated["websource"] == 'LINKEDIN':
        return extractDataLinkedin(metaCurated, metaTech)
    else:
        logger.warning(
            "{} : Type de websource inconnu {}. Faire évoluer le produit.".format(sys._getframe().f_code.co_name,
                                                                                  metaTech["websource"]))


def extractDataCreateCurated(metaTech):
    """
    Extraction des données pour les meta et pour la data à partir de la clé
    :param metaTech: metaTech pour accéder à la donnée
    :return:
    """
    logger.info("ENTER IN {} arg [{} {}]".format(sys._getframe().f_code.co_name, metaTech["id"], metaTech["websource"]))

    metaCurated = extractMeta(metaTech)

    extractData(metaCurated, metaTech)

    return metaCurated


def saveMeta(curatedMeta):
    """
    Sauvegarede des metaCurrated

    :param curatedMeta:
    :return:
    """
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    if not os.path.isdir(pathMetadata):
        logger.info("Creation du dossier meta {}".format(pathMetadata))
        os.makedirs(pathMetadata)
    metaFilePath = pathMetadata + "\\" + metaDataCurName
    csv_columns = curatedMeta.keys()
    writeHeader = not os.path.isfile(metaFilePath)

    try:
        with open(metaFilePath, 'a+', newline='', encoding="utf-8") as metafile:
            writer = csv.DictWriter(metafile, fieldnames=csv_columns, quoting=csv.QUOTE_ALL)
            if writeHeader:
                writer.writeheader()
            writer.writerow(curatedMeta)
            metafile.close()
        logger.debug("Meta pour {} sauvée dans {} ".format(curatedMeta["id"], metaFilePath))
    except:
        logger.error("Impossible d'ecrire dans le fichier METACURATED : " + sys.exc_info()[0])


def saveDataToCVS(curatedMeta, curatedData):
    """
    On sauve ici dans la bonne base pour chaque entreprise les donnée curated
    :param curatedData:
    :return:
    """
    logger.debug(
        "ENTER IN {} \n META : {} \n DATA : {}".format(sys._getframe().f_code.co_name, curatedMeta, curatedData))

    try:
        fileName = curatedMeta["websource"] + "_" + curatedMeta["type"] + "_" + curatedMeta[
            "entrepriseId"] + curatedNameSuffix
        logger.debug("Data pour {} sauvée dans {} ".format(curatedData["id"], fileName))
        logger.debug("\n DATA : \n {}".format(curatedData))

        dataFilePath = pathCurated + "\\" + curatedMeta["websource"] + "\\" + curatedMeta["type"] + "\\" + fileName

        if not os.path.isdir(os.path.dirname(dataFilePath)):
            logger.info("Creation du dossier data {}".format(os.path.dirname(dataFilePath)))
            os.makedirs(os.path.dirname(dataFilePath))

        logger.debug("FICHIER : {}".format(dataFilePath))
        csv_columns = curatedData.keys()
        writeHeader = not os.path.isfile(dataFilePath)
        logger.debug("Colonne : {}".format(csv_columns))
        with open(dataFilePath, 'a+', newline='', encoding="utf-8") as datafile:
            writer = csv.DictWriter(datafile, fieldnames=csv_columns, quoting=csv.QUOTE_ALL)
            if writeHeader:
                writer.writeheader()
                logger.debug("CREATION DES Colonnes : {}".format(csv_columns))
            writer.writerow(curatedData)
            datafile.close()
        logger.debug("data pour {} sauvée dans {} ".format(curatedMeta["id"], dataFilePath))
        curatedMeta.update({"db": dataFilePath})

    except:
        logger.error("Impossible d'ecrire dans le fichier DATA : " + sys.exc_info()[0])
        raise ValueError("Impossible d'ecrire dans le fichier DATA : " + sys.exc_info()[0])


def traitement():
    """
    boucle de Traitement principal
    :return:
    """
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    logger.debug("Traitement FROM : {} -> {}".format(pathLanding, pathCurated))
    while not os.path.isfile("web.stop"):
        inCurated = loadKeyInCurated()
        logger.debug("Liste deja en currated : {}".format(inCurated))
        listToProcess = getLisToProcess(inCurated)
        logger.debug("Liste à traiter : {}".format(listToProcess))
        logger.info("Nombre de fichier à traiter : {}".format(len(listToProcess)))

        for metaTech in listToProcess:
            try:
                logger.info("Meta {}".format(metaTech["id"]))
                curatedMeta = extractDataCreateCurated(metaTech)

                saveMeta(curatedMeta)
            except ValueError as vr:
                logger.warning("Problème de traitement pour la cle [{}] : {}".format(metaTech["id"], vr.message))
            except:
                logger.warning("Problème de traitement pour la cle [{}] : {}".format(metaTech["id"], sys.exc_info()[0]))

        t = 0
        logger.info("Prochain scan dans {} seconde(s)".format(timer - t))
        while t < timer:
            t = t + 1
            logger.debug("Prochain scan dans {} seconde(s)".format(timer - t))
            time.sleep(1)


# --------------------------------------
# programme Principal
# --------------------------------------

if __name__ == "__main__":
    print("*" * 80)
    print("PROGRAMME SOURCE WEB TO LANDING")
    print("*" * 80)
    # Recupération des arguments


    path, pause, lvlLog = parseArg(sys.argv[1:])

    setLogger(path,lvlLog)
    setDatalake(path)
    setTimer(pause)

    traitement()
