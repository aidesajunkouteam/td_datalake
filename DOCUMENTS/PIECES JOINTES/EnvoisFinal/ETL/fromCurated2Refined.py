# - * - coding: utf - 8 -
#
# chargement vers la refined
# usage
# fromCurated2Refined.py -t 30 -d "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\TD_DATALAKE\DATALAKE" -u donnemassive -p dm -b donnemassive -s localhost

import csv
import getopt
import logging
import os
import sys
import uuid

import mysql.connector
import time
import dateutil.parser
from datetime import datetime
import sqlite3
import re
import json

# Properties

pathDataLake = "../../DATALAKE/"

pathCurated = pathDataLake + "2_CURATED_ZONE"

curatedNameSuffix = "_curatedDb.csv"

pathLanding = pathDataLake + "1_LANDING_ZONE"

pathMetadata = pathDataLake + "METADATA"

metaDataCurName = "metaDataCurated.csv"

metaDataRefinedName = "metaDataRefined.csv"

pathFileError = pathDataLake + "ERRORFILE"

metaRefinedHeader = ['id', 'processDate', 'websource', 'entrepriseId', 'typefile', 'domaine', 'page', 'iddb', 'base',
                     'serveur', 'tablename', 'typedata']
timer = 30
## ACCES A AL BASE MYSQL
bdd = "donneemassive"
serveur = "localhost"
user = "user"
password = "password"

SCRIPT_NAME = os.path.basename(sys.argv[0])

# --------------------------------------
# Definition logger application
# --------------------------------------


logger = None

loggingLevel = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]


# stream_handler = logging.StreamHandler()
# stream_handler.setLevel(logging.INFO)
#
# logging.basicConfig(level=logging.DEBUG,
#                     format="%(asctime)s : [ %(levelname)s ] %(message)s",
#                     datefmt='%m/%d/%Y %H:%M:%S',
#                     handlers=[logging.FileHandler("Curated2Refined.log", mode='w',encoding="utf-8"),
#                               stream_handler])
#
# logger = logging.getLogger(SCRIPT_NAME)


# from logging.config import fileConfig
#
# fileConfig('logging_config.ini')
# logger = logging.getLogger()


# --------------------------------------
# definition des methodes et fonctions
# --------------------------------------
def usage(exitCode):
    """
    afficher le parametre pris en compte par l'application
    :param exitCode: code de sortie après le usage
    """
    logger.debug("usage : {} ")

    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print('      ex: {} [-h] [-d "path"] [-t seconde] [-s serveur] [-b base] [-u user] [-p password] [-l lvl]'.format(
        SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")
    print(" -t | --timer : \n"
          "              cadence de scanning.")
    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n"
          "               /!\\ réinitialisation des chemins par défaut pour les landing et sourceweb")
    print(" -s | --serveur : \n"
          "              hote du serveur mysql")
    print(" -b | --base : \n"
          "              nom de la base de données")
    print(" -u | --user : \n"
          "              nom de l'utilisateur de la base de donnée")
    print(" -p | --password : \n"
          "              mot de passe de l'utilisateur")
    print(" -l | --logger : \n"
          "              niveau de log {}. ".format(loggingLevel)
          )
    print("*" * 80)
    sys.exit(exitCode)


#################
def setCurated(path):
    """
    initialisation de webSourcePath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    logger.debug("Definition chemin curated {}".format(path))
    global pathCurated
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathCurated = path
    else:
        logger.info("Creation du dossier de curatedZone [{}] ".format(path))
        try:
            os.makedirs(path)
            pathCurated = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setLanding(path):
    """
    initialisation de LandingPath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathLanding
    logger.debug("Definition chemin landing")
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathLanding = path
    else:
        logger.info("Creation du dossier de landing [{}] ".format(path))
        try:
            os.makedirs(path)
            pathLanding = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setTimer(value):
    """
      initialisation du timer de scan
      :param value: valeur du timer
      :raise ValueError si timer  inférieur ou égal à 0
      """
    global timer

    logger.debug("Definition timer de scanning")
    if int(value) >= 0:
        timer = int(value)
    else:
        raise ValueError("Valeur pour le timer invalide ")


#################
def setMetadata(path):
    """
    initialisation de metapath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathMetadata
    logger.debug("Definition chemin meta")
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathMetadata = path
    else:
        logger.info("Creation du dossier de meta [{}] ".format(path))
        try:
            os.makedirs(path)
            pathMetadata = path
        except:
            raise IOError("Impossible de construire {}".format(path))


#################
def setErrorFile(path):
    """
    initialisation de errorFile
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathFileError
    logger.debug("Definition chemin errorFile")
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathFileError = path
    else:
        logger.info("Creation du dossier de errorFile [{}] ".format(path))
        try:
            os.makedirs(path)
            pathFileError = path
        except:
            raise IOError("Impossible de construire {}".format(path))


##################
def setDatalake(path):
    """
         initialisation du path datalake
         :param value: chemin du datalake
         :raise ValueError si path invalide
    """
    global pathDataLake

    logger.debug("Definition chemin datalake [ {} ]".format(path))
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathDataLake = path
        setCurated(pathDataLake + "\\2_CURATED_ZONE")
        setLanding(pathDataLake + "\\1_LANDING_ZONE")
        setMetadata(pathDataLake + "\\METADATA")
        setErrorFile(pathDataLake + "\\ERRORFILE")
    else:
        raise ValueError("Chemin datalake des datas non valide : {}".format(path))


##########################################
def setLogger(datalakePath, lvl):
    """
       initialisation de dossierLogFile
       :param path: valeur de path
       :raise ValueError si chemin non valide
       """
    global logger

    if os.path.exists(datalakePath) and (not os.path.isfile(datalakePath)):
        pathlog = os.path.join(datalakePath, "LOG")

        # logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, path))
        if os.path.exists(pathlog) and (not os.path.isfile(pathlog)):
            pathlog = pathlog
        else:
            print("Creation du dossier de log [{}] ".format(pathlog))
            try:
                os.makedirs(pathlog)
                pathlog = pathlog
            except:
                raise IOError("Impossible de construire {}".format(pathlog))

        pathfilelog = os.path.join(pathlog, "LOG_" + SCRIPT_NAME.upper() + ".log")
        if not lvl in loggingLevel:
            lvl = 'INFO'
        else:
            lvl = lvl.upper()

        print("Niveau Logger " + lvl)
        print("Fichier logger " + pathfilelog)

        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(lvl)

        file_handler = logging.FileHandler(filename=pathfilelog, mode="w", encoding="utf-8")
        file_handler.setLevel(logging.DEBUG)

        logging.basicConfig(level=logging.DEBUG,
                            format="%(asctime)s : [ %(levelname)s ] %(message)s",
                            datefmt='%m/%d/%Y %H:%M:%S',
                            handlers=[file_handler,
                                      stream_handler])

        logger = logging.getLogger(SCRIPT_NAME)
        logger.info("LOGGER ACTIF....")

        print("Activation du logger ")
    else:
        print("Impossible de construire le chemin du fichier log sur {}".format(datalakePath))
        raise IOError("Impossible de construire le chemin du fichier sur log {}".format(datalakePath))


##################
def setUser(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name, arg))
    global user
    if arg != "":
        user = arg


def setPassword(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name, arg))
    global password
    if arg != "":
        password = arg


def setServeur(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name, arg))
    global serveur
    if arg != "":
        serveur = arg


def setDataBase(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name, arg))
    global bdd
    if arg != "":
        bdd = arg


def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param appli: nom de l'application
    :return:
    """

    try:
        opts, args = getopt.getopt(argv, "hd:t:u:p:s:b:l:",
                                   ["help", "datalake=", "timer=", "user=", "password=", "serveur=", "base=",
                                    "logger="])
    except getopt.GetoptError:
        usage(2)

    path = pathDataLake
    pause = timer
    lvlLog = 'DEBUG'
    u = user
    p = password
    b = bdd
    s = serveur

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-t", "--timer"):
            pause = arg
        elif opt in ("-d", "--datalake"):
            path = arg
        elif opt in ("-u", "--user"):
            u = arg
        elif opt in ("-p", "--password"):
            p = arg
        elif opt in ("-s", "--serveur"):
            s = arg
        elif opt in ("-b", "--base"):
            b = arg
        elif opt in ("-l", "--logger"):
            lvlLog = arg
        else:
            usage(1)

    return path, pause, lvlLog, u, p, b, s


# --------------------------------------
# programme Principal
# --------------------------------------

def connectToMysqlDb():
    """
    Connection à la base de données
    :return: une connection à la base
    :raise BAseExcetion en cas de problème
    """
    try:
        mydbConnection = mysql.connector.connect(
            host=serveur,
            user=user,
            passwd=password,
            database=bdd
        )

        logger.info("Connection à {} sur serveur {} user {} OK".format(bdd, serveur, user))
        return mydbConnection
    except:
        raise BaseException("Impossible de se connecter à la base de donnée. ")


def closeMysqlDb(mydbConnection):
    """
    Cloture de la connection à la base de donnée mysql
    :param mydbConnection:
    :param mycursor:
    :return:
    """
    if mydbConnection.is_connected():
        mydbConnection.close()


def getSqliteConnexion(memory=False):
    # TODO a passer à :memory: pour accelerer la chose mais gaffe a la memoire
    # logger.debug("{} - {}".format(sys._getframe().f_code.co_name, memory))
    return sqlite3.connect('metaRefined.db.tmp')


def loadMetaRefined():
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()

    cursor.execute("DROP TABLE IF EXISTS refined;")

    sep = ","
    cols = sep.join(metaRefinedHeader)
    cols = cols.replace(',', ' text,')
    cols = cols + " text"

    sql = """CREATE TABLE IF NOT EXISTS refined (
                    id text,
                    processDate text,
                    websource text,
                    entrepriseId text,
                    `typefile` text,
                    domaine text,
                    page text,
                    iddb text,
                    base text,
                    serveur text,
                    `tablename` text,
                    typedata text
                    );"""
    logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
    cursor.execute(sql)

    metaRefinedFile = os.path.join(pathMetadata, metaDataRefinedName)

    if os.path.exists(metaRefinedFile):
        with open(metaRefinedFile, 'r+', newline='', encoding="utf-8") as csvFile:
            logger.info("{} : Import Refined en SQLITE ".format(sys._getframe().f_code.co_name))
            reader = csv.DictReader(csvFile)
            cols = ",".join(metaRefinedHeader)
            sql = "INSERT INTO refined VALUES (?,?,?,?,?,?,?,?,?,?,?,?);".format(cols)
            logger.debug("{} SQL {}".format(sys._getframe().f_code.co_name, sql))
            for row in reader:
                cursor.execute(sql, list(row.values()))

    cursor.execute("SELECT COUNT(*) FROM refined")

    res = cursor.fetchone()

    logger.debug("Taille refined {}".format(res[0]))

    sqLiteConn.commit()
    sqLiteConn.close()


def isIdItemInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))
    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()
    sql = "SELECT COUNT(*) from refined where iddb='{}';".format(id)

    cursor.execute(sql)
    res = cursor.fetchone()[0]
    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res == 1))
    sqLiteConn.commit()
    sqLiteConn.close()
    return res == 1


def isEnterpriseInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()

    sql = "SELECT COUNT(*) from refined where iddb='{}' and tablename='dim_entreprise';".format(id)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchone()[0]
    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res >= 1))
    sqLiteConn.commit()
    sqLiteConn.close()
    return res >= 1





def isDbCuratedInRefined(id):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()
    sql = "SELECT COUNT(*) from refined where id='{}'".format(id)

    cursor.execute(sql)
    res = cursor.fetchone()[0]

    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res >= 1))
    sqLiteConn.commit()
    sqLiteConn.close()

    return res >= 1


def saveAvisSoc(row, MetaDb):
    try:

        # verification si deja en base..... via currated et meta refined.... ou duplicate Key.....
        logger.debug("ENTER IN {} entreprise {} - {}".format(sys._getframe().f_code.co_name, row["entrepriseId"],
                                                             row["entreprise"]))
        cursor = None
        if isIdItemInRefined(row["entrepriseId"]):
            logger.info(
                "{} ABANDON raffinage déjà effectué de la ligne [{}]".format(sys._getframe().f_code.co_name, row))
            return False

        cursor = mydbconnection.cursor()
        sql = "insert into dim_entreprise (id_entreprise,nom_entreprise) values (%s,%s)"
        values = (row["entrepriseId"], str(row["entreprise"]).upper())
        logger.debug("{} entreprise Requete {} - {}".format(sys._getframe().f_code.co_name, row["entrepriseId"],
                                                            row["entreprise"]))
        cursor.execute(sql, values)
        mydbconnection.commit()
        logger.debug("{} entreprise chargé en Base {} - {}".format(sys._getframe().f_code.co_name, row["entrepriseId"],
                                                                   row["entreprise"]))
        metaRefined = {
            'id': row["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': row['websource'],
            'entrepriseId': row['entrepriseId'],
            'typefile': MetaDb["type"],
            'domaine': MetaDb["domaine"],
            'page': str(row["page"]),
            'iddb': row["entrepriseId"],
            'base': bdd,
            'serveur': serveur,
            'tablename': 'dim_entreprise',
            'typedata': MetaDb["domaine"]
        }

        logger.debug("{} MetaRafined construite {} ".format(sys._getframe().f_code.co_name, metaRefined))

        saveMetaRefined(metaRefined)

        return True

    except mysql.connector.Error as error:
        logger.debug("{} | SqlError en BdD {}".format(sys._getframe().f_code.co_name, error))
        if error.errno != 1062:
            # pas une duplicate y a un pb
            logger.warning("{} | SqlError en BdD {}".format(sys._getframe().f_code.co_name, error))
            raise ValueError("{} | SqlError en BdD {}".format(sys._getframe().f_code.co_name, error))
    except TypeError as err:
        logger.warning("{} | TypeError en BdD {}".format(sys._getframe().f_code.co_name, err))
        raise ValueError("{} | TypeError en BdD {}".format(sys._getframe().f_code.co_name, err))
    except Exception as err:
        logger.warning("{} | EXCEPTION BdD {}".format(sys._getframe().f_code.co_name, err))
        raise ValueError("{} | EXCEPTION en BdD {}".format(sys._getframe().f_code.co_name, err))
    except:
        logger.warning("{} | ERREUR INCONNU BdD {}".format(sys._getframe().f_code.co_name))
        raise ValueError("{} | ERREUR INCONNU en BdD {}".format(sys._getframe().f_code.co_name))
    finally:
        if (mydbconnection.is_connected()):
            if cursor != None:
                cursor.close()


def createDateAvis(MetaDb, row):
    logger.debug(
        "ENTER IN {} arg [{}]".format(sys._getframe().f_code.co_name, row["avisdate"]))

    mydate = dateutil.parser.parse(row["avisdate"])
    date = mydate.strftime("%Y-%m-%d")
    datekey = mydate.strftime("%Y_%j")

    if isIdItemInRefined(datekey):
        return datekey, date

    try:

        cursor = mydbconnection.cursor()
        sql = "insert into dim_calendar (id_date,calendar) values (%s,%s)"
        values = (datekey, date)

        cursor.execute(sql, values)
        mydbconnection.commit()

        metaRefined = {
            'id': row["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': row['websource'],
            'entrepriseId': row['entrepriseId'],
            'typefile': MetaDb["type"],
            'domaine': MetaDb["domaine"],
            'page': str(row["page"]),
            'iddb': datekey,
            'base': bdd,
            'serveur': serveur,
            'tablename': 'dim_calendar',
            'typedata': 'Date'
        }
        saveMetaRefined(metaRefined)
        return datekey, date

    except mysql.connector.Error as error:
        logger.warning("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
    finally:
        if (mydbconnection.is_connected()):
            cursor.close()


def saveAvisAvis(row, MetaDb):
    logger.debug(
        "ENTER IN {} Avis {} - {}".format(sys._getframe().f_code.co_name, row["avisid"], row["avisnote"]))
    try:
        # construction de la requete d'insertion de l'avis

        if isIdItemInRefined(row["avisid"]):
            logger.info(
                "{} ABANDON raffinage déjà effectué de la ligne [{}]".format(sys._getframe().f_code.co_name, row))
            return False

        datekey, date = createDateAvis(MetaDb, row)

        cursor = mydbconnection.cursor()
        sql = "insert into fact_score (id_avis,id_entreprise,id_date,score,avis) values (%s,%s,%s,%s,%s)"
        values = (row["avisid"], row["entrepriseId"], datekey, row["avisnote"], row["commentaires"])

        cursor.execute(sql, values)
        mydbconnection.commit()

        metaRefined = {
            'id': row["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': row['websource'],
            'entrepriseId': row['entrepriseId'],
            'typefile': MetaDb["type"],
            'domaine': MetaDb["domaine"],
            'page': str(row["page"]),
            'iddb': row["avisid"],
            'base': bdd,
            'serveur': serveur,
            'tablename': 'fact_score',
            'typedata': MetaDb["type"]
        }
        saveMetaRefined(metaRefined)

        logger.debug("{} Avis en Base {} - {}".format(sys._getframe().f_code.co_name, row["entrepriseId"],
                                                      row["entreprise"]))

        return True
    except mysql.connector.Error as error:
        logger.warning("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
        raise ValueError("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
    except:
        raise ValueError("{} | Echec insertion en BdD ".format(sys._getframe().f_code.co_name))
    finally:
        if (mydbconnection.is_connected()):
            cursor.close()


def processAvisLine(row, MetaDb):
    """
    Lecture d'une ligne de la base de données Curated pour la traiter et l'insert en base Score d'une entreprise
    :param row: ligne à traiter
    :return: None
    """
    logger.info("{} raffinage de la ligne [{} {}]".format(sys._getframe().f_code.co_name, row["id"], row["avisid"]))

    if isIdItemInRefined(row["avisid"]):
        logger.info(
            "{} ABANDON raffinage déjà effectué de la ligne [{} {}]".format(sys._getframe().f_code.co_name, row["id"],
                                                                            row["avisid"]))
        return False

    try:
        # on traite l entreprise
        saveAvisSoc(row, MetaDb)
        # on traite les avis
        saveAvisAvis(row, MetaDb)

    except ValueError as error:
        logger.warning("Problème pendant le traitemlent de {} : {}".format(row, error))


def extractGeoDataGlassdoor(param):
    """
    Extraction localisation Glassdoor
    :param param:
    :return: ville, pays
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, param))
    pays = "FRANCE"
    ville = param
    if "(" in param:
        match = re.match("(.*)\((.*)\)", param)
        ville = match.group(1).upper()
        pays = match.group(2).upper()

    # On garde ce qui est avant la virgule s'il y en a une
    ville = str(ville.split(',')[0]).upper()

    return ville, pays


def extractGeoDataLinked(param):
    """
    Extraction localisation LINKEDIN
    :param param:
    :return: ville, pays
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, param))

    pays = "FRANCE"
    ville = param.upper().strip()

    # On garde ce qui est avant la virgule s'il y en a une
    if "," in param:
        parts = param.split(',')
        logger.debug("{} args [{}]".format(sys._getframe().f_code.co_name, parts))
        ville = str(parts[0]).upper().strip()
        if len(parts) > 1 :
            pays = parts[len(parts) - 1].upper().strip()

    if pays.strip().upper() == 'FR':
        pays = "FRANCE"

    return ville.strip().upper(), pays.strip().upper()


def processInfoLine(row, MetaDb):
    """
       Lecture d'une ligne de la base de données Curated pour la traiter et l'insert en base information sur une entreprise
       :param row: ligne à traiter
       :return: meta de la ligne
       """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, row))

    # on ne retraite pas un id
    if isDbCuratedInRefined(row['id']):
        logger.info("{} ABANDON raffinage déjà effectué de la ligne [{}]".format(sys._getframe().f_code.co_name, row))
        return False

    metaRefined = None
    try:
        # on traite l entreprise
        cursor = mydbconnection.cursor()
        newId = row['entrepriseId']
        updating = False
        # si deja present en table dim donc updating
        if isEnterpriseInRefined(row["entrepriseId"]):
            updating = True

        logger.debug("{} - mode mise à jour [{}]".format(sys._getframe().f_code.co_name, updating))

        ville, pays = extractGeoDataGlassdoor(row["Siège social"])

        # Vérification si déjà en base via les metaRefined table via les champs dim_entreprise/iddb
        if updating:
            # Oui on Update
            sql = "update dim_entreprise set nom_entreprise = %s, SiegeSocial=%s , pays=%s where id_entreprise = %s"
            values = (str(row["entreprise"]).upper(), ville, pays, row["entrepriseId"])
        else:
            sql = "insert into dim_entreprise (id_entreprise,nom_entreprise, SiegeSocial,pays) values (%s,%s,%s,%s)"
            values = (str(row["entrepriseId"]).upper(), str(row["entreprise"]).upper(), ville, pays)

        # logger.debug("{} - UPD [{}]".format(sys._getframe().f_code.co_name, updating))
        # logger.debug("{} - SQL [{}]".format(sys._getframe().f_code.co_name, sql))
        # logger.debug("{} - VAL [{}]".format(sys._getframe().f_code.co_name,  values))

        cursor.execute(sql, values)
        mydbconnection.commit()

        metaRefined = {
            'id': row["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': row['websource'],
            'entrepriseId': row['entrepriseId'],
            'typefile': MetaDb["type"],
            'domaine': MetaDb["domaine"],
            'page': str(row["page"]),
            'iddb': row["entrepriseId"],
            'base': bdd,
            'serveur': serveur,
            'tablename': 'dim_entreprise',
            'typedata': MetaDb["domaine"]
        }
        logger.debug("{} info soc en Base {} - {}".format(sys._getframe().f_code.co_name, row["entrepriseId"],
                                                          row["entreprise"]))

        saveMetaRefined(metaRefined)

    except mysql.connector.Error as error:
        logger.warning("{} | Mysql Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
        raise ValueError("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
    except Exception as ex:
        raise ValueError("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, ex))
    finally:
        if (mydbconnection.is_connected()):
            if not cursor == None:
                cursor.close()


def isFonctionInRefined(fonc):
    """
    Verification si cette fonction à deja était mise en base
    :param fonc:
    :return:
    """
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, fonc))

    # sqliteExport("SELECT * from refined where iddb='{}'  and tablename='dim_fonctions' and typedata='FONCTIONS';".format(id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()



    sql = "SELECT iddb from refined where tablename='dim_fonctions' and typedata='{}';".format(
        fonc)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchall()
    if not res:
        logger.debug("{} = {} NONE".format(sys._getframe().f_code.co_name, res))
        return None

    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res))

    sqLiteConn.commit()
    sqLiteConn.close()

    return res[0][0]


def insertEmpFonctions(row, metaDb):
    """
    Ajout des fonctions d'un emplois
    :param row:
    :param cursor:
    :return:
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, row["fonctions"]))
    # PARSING
    keys = []
    fonctions = json.loads(row["fonctions"])
    print(type(fonctions))
    print(fonctions)
    # verif si en base.ie
    for fonc in fonctions.values():
        fonc = fonc.strip()
        foncKey = isFonctionInRefined(fonc)
        logger.debug("{} args [{}]".format(sys._getframe().f_code.co_name, foncKey))
        if foncKey != None:
            keys.append(foncKey)
            continue

        logger.debug("{} Nouvelle fonction [{}]".format(sys._getframe().f_code.co_name, row["fonctions"]))
        foncKey = str(uuid.uuid4())
        cursor = mydbconnection.cursor()
        sql = "insert into dim_fonctions (id_fonction,fonction) values (%s,%s);"
        placeholders = (foncKey, fonc)
        cursor.execute(sql, placeholders)
        mydbconnection.commit()

        metaRefined = {
            'id': metaDb["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': metaDb['websource'],
            'entrepriseId': row['emploisId'],
            'typefile': metaDb["type"],
            'domaine': metaDb["domaine"],
            'page': str(metaDb["page"]),
            'iddb': foncKey,
            'base': bdd,
            'serveur': serveur,
            'tablename': 'dim_fonctions',
            'typedata': fonc
        }

        saveMetaRefined(metaRefined)
        keys.append(foncKey)

    return keys


def isLocalisationInRefined(loc):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, loc))

    # sqliteExport("SELECT * from refined where iddb='{}'  and tablename='dim_fonctions' and typedata='FONCTIONS';".format(id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()

    sql = "SELECT iddb from refined where tablename='dim_localisation' and typedata='{}';".format(
        loc)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchall()
    if not res:
        logger.debug("{} = {} NONE".format(sys._getframe().f_code.co_name, res))
        return None

    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res))

    sqLiteConn.commit()
    sqLiteConn.close()

    return res[0][0]


def insertEmpLocalisation(row, metaDb):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, row["localisation"]))
    # PARSING
    ville, pays = extractGeoDataLinked(row["localisation"])

    logger.debug("{} localisation [{} / {} ]".format(sys._getframe().f_code.co_name, ville, pays))

    # verif si en base
    localisation = ville+" "+pays
    localisation = localisation.upper()

    locKey = isLocalisationInRefined(localisation)

    if locKey != None:
        return locKey

    logger.debug("{} Nouvelle Localisation [{}]".format(sys._getframe().f_code.co_name, localisation))
    locKey = str(uuid.uuid4())
    cursor = mydbconnection.cursor()
    sql = "insert into dim_localisation (id_localisation,ville,pays) values (%s,%s,%s);"
    placeholders = (locKey, ville,pays)
    cursor.execute(sql, placeholders)
    mydbconnection.commit()

    metaRefined = {
        'id': metaDb["id"],
        'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
        'websource': metaDb['websource'],
        'entrepriseId': row['emploisId'],
        'typefile': metaDb["type"],
        'domaine': metaDb["domaine"],
        'page': str(metaDb["page"]),
        'iddb': locKey,
        'base': bdd,
        'serveur': serveur,
        'tablename': 'dim_localisation',
        'typedata': localisation
    }

    saveMetaRefined(metaRefined)


    return locKey


def isSecteurInRefined(secteur):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, secteur))

    # sqliteExport("SELECT * from refined where iddb='{}'  and tablename='dim_fonctions' and typedata='FONCTIONS';".format(id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()

    sql = "SELECT iddb from refined where tablename='dim_secteurs' and typedata='{}';".format(
        secteur)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchall()
    if not res:
        logger.debug("{} = {} NONE".format(sys._getframe().f_code.co_name, res))
        return None

    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res))

    sqLiteConn.commit()
    sqLiteConn.close()

    return res[0][0]


def insertEmpSecteurs(row, metaDb):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, row["secteurs"]))
    # PARSING
    keys = []
    secteurs = json.loads(row["secteurs"])
    print(type(secteurs))
    print(secteurs)
    # verif si en base.ie
    for secteur in secteurs.values():
        secteur = secteur.strip()
        secKey = isSecteurInRefined(secteur)
        logger.debug("{} args [{}]".format(sys._getframe().f_code.co_name, secKey))
        if secKey != None:
            keys.append(secKey)
            continue

        logger.debug("{} Nouveau secteur [{}]".format(sys._getframe().f_code.co_name, row["secteurs"]))
        secKey = str(uuid.uuid4())
        cursor = mydbconnection.cursor()
        sql = "insert into dim_secteurs (id_secteur,secteur) values (%s,%s);"
        placeholders = (secKey, secteur)
        cursor.execute(sql, placeholders)
        mydbconnection.commit()

        metaRefined = {
            'id': metaDb["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': metaDb['websource'],
            'entrepriseId': row['emploisId'],
            'typefile': metaDb["type"],
            'domaine': metaDb["domaine"],
            'page': str(metaDb["page"]),
            'iddb': secKey,
            'base': bdd,
            'serveur': serveur,
            'tablename': 'dim_secteurs',
            'typedata': secteur
        }

        saveMetaRefined(metaRefined)
        keys.append(secKey)

    return keys


def isTypeEmpInRefined(typeEmp):
    logger.debug("{} - {}".format(sys._getframe().f_code.co_name, typeEmp))

    # sqliteExport("SELECT * from refined where iddb='{}'  and tablename='dim_fonctions' and typedata='FONCTIONS';".format(id))

    sqLiteConn = getSqliteConnexion()
    cursor = sqLiteConn.cursor()

    sql = "SELECT iddb from refined where tablename='dim_type' and typedata='{}';".format(
        typeEmp)

    logger.debug(sql)
    cursor.execute(sql)

    res = cursor.fetchall()
    if not res:
        logger.debug("{} = {} NONE".format(sys._getframe().f_code.co_name, res))
        return None

    logger.debug("{} = {} ".format(sys._getframe().f_code.co_name, res))

    sqLiteConn.commit()
    sqLiteConn.close()

    return res[0][0]


def insertEmpType(row, metaDb):
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, row["typeEmplois"]))


    # verif si en base.ie
    typeEmp=row["typeEmplois"].strip()
    typeKey = isTypeEmpInRefined(typeEmp)

    if typeKey != None:
        return typeKey

    logger.debug("{} Nouveu Type [{}]".format(sys._getframe().f_code.co_name, row["typeEmplois"]))
    typeKey = str(uuid.uuid4())
    cursor = mydbconnection.cursor()
    sql = "insert into dim_type (id_type,nom) values (%s,%s);"
    placeholders = (typeKey, typeEmp)
    cursor.execute(sql, placeholders)
    mydbconnection.commit()

    metaRefined = {
        'id': metaDb["id"],
        'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
        'websource': metaDb['websource'],
        'entrepriseId': row['emploisId'],
        'typefile': metaDb["type"],
        'domaine': metaDb["domaine"],
        'page': str(metaDb["page"]),
        'iddb': typeKey,
        'base': bdd,
        'serveur': serveur,
        'tablename': 'dim_type',
        'typedata': typeEmp
    }

    saveMetaRefined(metaRefined)

    return typeKey



def processEmpLine(row, MetaDb):
    """
    Process d'une information LINKEDID
    :param row:
    :param MetaDb:
    :return:
    """
    logger.debug("ENTER IN {} args [{} {}]".format(sys._getframe().f_code.co_name,  row['id'],row['emploisId']))

    # on ne retraite pas un fichier deja traité
    if isDbCuratedInRefined(row['id']):
        logger.info("{} ABANDON raffinage déjà effectué pour [{}]".format(sys._getframe().f_code.co_name, row['id']))
        return False

    metaRefined = None
    cursor = None
    try:
        # on traite l emploi

        newId = row['emploisId']
        titreEmp = row["titreEmplois"].strip().title()
        entreprise = row['entreprise'].strip().upper()
        commentaire = row['commentaire'].strip()

        # si deja present en table dim donc updating
        if isIdItemInRefined(newId):
            logger.info(
                "{} ABANDON raffinage emplois déjà effectué pour [{}]".format(sys._getframe().f_code.co_name, newId))
            return False

        # INSERTIONS LOCALISATION

        localisationKey = insertEmpLocalisation(row, MetaDb)
        logger.debug("{} localisation Keys [{}]".format(sys._getframe().f_code.co_name, localisationKey))
        # Vérification si déjà en base via les metaRefined table via les champs dim_entreprise/iddb

        # INSERTION FONCTIONS

        fonctionsKeys = insertEmpFonctions(row, MetaDb)
        logger.debug("{} fonctions Keys [{}]".format(sys._getframe().f_code.co_name, fonctionsKeys))


        # INSERTION FONCTIONS

        secteursKeys = insertEmpSecteurs(row, MetaDb)
        logger.debug("{} secteurs Keys [{}]".format(sys._getframe().f_code.co_name, secteursKeys))

        # INSERTIONS TypeEmp

        typeEmpKey = insertEmpType(row, MetaDb)
        logger.debug("{} type Key [{}]".format(sys._getframe().f_code.co_name, typeEmpKey))



        # INSERTION EMPLOI

        cursor = mydbconnection.cursor()
        sql = """insert into fact_emp (id_employe,id_type, id_entreprise,id_localisation,intitule,commentaire) values (%s,%s,%s,%s,%s,%s)"""
        values = (newId, typeEmpKey, entreprise, localisationKey, titreEmp,commentaire)

        cursor.execute(sql, values)


        # INSERTION RELATION FONCTIONS-EMP
        for fk in fonctionsKeys:
            sql = """insert into dim_fonction_emp (id_fonction,id_employe) values (%s,%s)"""
            values = (fk,newId)
            cursor.execute(sql, values)

        # INSERTION RELATION SECTEURS-EMP
        for sk in secteursKeys:
            sql = """insert into dim_secteur_emp (id_secteur,id_employe) values (%s,%s)"""
            values = (sk,newId)
            cursor.execute(sql, values)

        # logger.debug("{} - UPD [{}]".format(sys._getframe().f_code.co_name, updating))
        # logger.debug("{} - SQL [{}]".format(sys._getframe().f_code.co_name, sql))
        # logger.debug("{} - VAL [{}]".format(sys._getframe().f_code.co_name,  values))

        mydbconnection.commit()

        metaRefined = {
            'id': row["id"],
            'processDate': datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            'websource': row['websource'],
            'entrepriseId': newId,
            'typefile': MetaDb["type"],
            'domaine': MetaDb["domaine"],
            'page': str(row["page"]),
            'iddb': newId,
            'base': bdd,
            'serveur': serveur,
            'tablename': 'fact_emp',
            'typedata': MetaDb["domaine"]
        }
        logger.debug("{} iemp en Base {} - {}".format(sys._getframe().f_code.co_name, newId,
                                                          titreEmp))

        saveMetaRefined(metaRefined)

    except mysql.connector.Error as error:
        logger.warning("{} | Mysql Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
        raise ValueError("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, error))
    except Exception as ex:
        raise ValueError("{} | Echec insertion en BdD {}".format(sys._getframe().f_code.co_name, ex))
    finally:
        if (mydbconnection.is_connected()):
            if not cursor == None:
                cursor.close()


def processToRefine(MetaDb):
    """
    Traitement des MetaCurrated pour extraire les refined depuis les dataCurated
    :param MetaDb: db à traiter
    :return: metarefined: liste des metas des données rafiné
    """
    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, MetaDb))
    if os.path.exists(MetaDb["db"]):
        with open(MetaDb["db"], 'r+', newline='', encoding="utf-8") as csvFile:
            logger.info("{} : Rafinage de {} ".format(sys._getframe().f_code.co_name, MetaDb["db"]))
            reader = csv.DictReader(csvFile)
            try:
                for row in reader:
                    if MetaDb["websource"] == 'GLASSDOOR' and MetaDb["type"] == "AVIS":
                        processAvisLine(row, MetaDb)
                    elif MetaDb["websource"] == 'GLASSDOOR' and MetaDb["type"] == "INFO":
                        processInfoLine(row, MetaDb)
                    elif MetaDb["websource"] == 'LINKEDIN' and MetaDb["type"] == "INFO":
                        processEmpLine(row, MetaDb)
                    else:
                        logger.warning(
                            "NON PRIS EN CHARGE EN COURS IMPLEMENTATION {}-{} \n{}".format(MetaDb["websource"],
                                                                                           MetaDb["type"],
                                                                                           MetaDb))
            except NotImplementedError as err:
                logger.warning("{} : [{}] {} ".format(sys._getframe().f_code.co_name, MetaDb["db"], err))

    else:
        raise FileNotFoundError("Fichier {} non trouvé".format(MetaDb["db"]))


def saveMetaRefined(metaRefined):
    """
    Sauvegarde des metaRefined.
    
    :param metaRefined: 
    :return: 
    """

    logger.debug("ENTER IN {} args [{}]".format(sys._getframe().f_code.co_name, metaRefined))
    if not os.path.isdir(pathMetadata):
        logger.info("Creation du dossier meta {}".format(pathMetadata))
        os.makedirs(pathMetadata)
    metaFilePath = pathMetadata + "\\" + metaDataRefinedName
    csv_columns = metaRefined.keys()
    writeHeader = not os.path.isfile(metaFilePath)

    try:
        with open(metaFilePath, 'a+', newline='', encoding="utf-8") as metafile:
            writer = csv.DictWriter(metafile, fieldnames=csv_columns, quoting=csv.QUOTE_ALL)
            if writeHeader:
                writer.writeheader()
            writer.writerow(metaRefined)
            metafile.close()
            logger.debug("{} - MetaRefined pour {} sauvée dans {} ".format(sys._getframe().f_code.co_name, metaRefined,
                                                                           metaFilePath))

        logger.debug("{} - MetaRefined pour {} Maj en local ".format(sys._getframe().f_code.co_name, metaRefined))

        try:

            sqLiteConn = getSqliteConnexion()

            cursor = sqLiteConn.cursor()
            columns = ', '.join(metaRefined.keys())
            placeholders = ', '.join('?' * len(metaRefined))
            sql = "INSERT INTO refined VALUES ({});".format(placeholders)
            logger.debug("{} - {} ".format(sys._getframe().f_code.co_name, sql))
            cursor.execute(sql, list(metaRefined.values()))

            sqLiteConn.commit()
        except sqlite3.Error as e:
            logger.error("SQlite Database error: %s" % e)
        except Exception as e:
            logger.error("SQlite Exception in _query: %s" % e)
        except TypeError as e:
            logger.error("SQlite TypeError in _query: %s" % e)
        finally:
            sqLiteConn.close()

        logger.debug("{} - DATA [] {}".format(sys._getframe().f_code.co_name, metaRefined))


    except:
        logger.error("Impossible d'ecrire dans le fichier METAREFINED : " + sys.exc_info()[0])
        raise ValueError("Impossible d'ecrire dans le fichier METAREFINED ")


def getDbToProcess():
    """
    Retourne la liste des base de données curated à traiter
    :return: la liste des curated qui sont à traiter
    """
    logger.debug("ENTER IN {} ".format(sys._getframe().f_code.co_name))
    metaCurated = pathMetadata + "\\" + metaDataCurName
    listToRefine = []
    if os.path.exists(metaCurated):
        logger.debug("{} chargement depuis {}".format(sys._getframe().f_code.co_name, metaDataCurName))
        with open(metaCurated, 'r+', newline='', encoding="utf-8") as csvFile:
            reader = csv.DictReader(csvFile)
            for row in reader:
                if not isDbCuratedInRefined(row['id']):
                    listToRefine.append(row)

    logger.debug("OUT OFF {} args [{}]".format(sys._getframe().f_code.co_name, listToRefine))

    return listToRefine


def traitement():
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name))
    global mydbconnection
    try:
        while not os.path.isfile("refined.stop"):
            mydbconnection = connectToMysqlDb()

            loadMetaRefined()
            dbToProcess = getDbToProcess()
            logger.info(
                "{} : il y a {} base curated à traiter ".format(sys._getframe().f_code.co_name, len(dbToProcess)))
            for db in dbToProcess:
                try:
                    processToRefine(db)
                except Exception as ex:
                    logger.warning("Problème pour le traitement de {} : {}".format(db, ex.message))
                except NotImplementedError as nyierr:
                    logger.warning("Problème de mise à jour du programme {} : {}".format(db, nyierr))
                except TypeError as err:
                    logger.warning("Problème pour le traitement de {} : {}".format(db, err.message))

            closeMysqlDb(mydbconnection)

            t = 0
            logger.info("Prochain scan dans {} seconde(s)".format(timer - t))
            while t < timer:
                t = t + 1
                logger.debug("Prochain scan dans {} seconde(s)".format(timer - t))
                time.sleep(1)
    except:
        logger.error("Erreur dans le traitement ! ARRET DU PROGRAMME ." + sys.exc_info()[0])

    raise NotImplementedError("! NON IMPLEMENTE ! ")


if __name__ == "__main__":
    print("*" * 80)
    print("PROGRAMME CURATED TO REFINED")
    print("*" * 80)
    print()

    # Recupération des arguments
    path, pause, lvlLog, usr, pwd, base, serv = parseArg(sys.argv[1:])

    setLogger(path, lvlLog)
    setDatalake(path)
    setServeur(serv)
    setDataBase(base)
    setTimer(pause)
    setUser(usr)
    setPassword(pwd)

    traitement()
