# - * - coding: utf - 8 -

# initDatalake.py -o "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\DEPOT" -d "C:\Users\florent\Documents\-=BIBD=-\GoogleDrive\BI_BD\DataLake\TD_DATALAKE\DATALAKE" -u donnemassive -p dm -b donnemassive -s localhost

import os
import sys
import getopt
import shutil
import mysql
from mysql import connector

pathDataLake=None
webSource="0_SOURCE_WEB"
landing="1_LANDING_ZONE"
curated="2_CURATED_ZONE"
error="ERRORFILE"
metaDir="METADATA"
metaFilesDict={"tec": "metaDataTech.csv",
           "cur":"metaDataCurated.csv",
           "ref":"metaDataRefined.csv"}
pathOrigine=None

bdd="donneemassive"
serveur="localhost"
user="user"
password="password"

SCRIPT_NAME = os.path.basename(sys.argv[0])

def dropDir(path):
    if os.path.isdir(path):
        shutil.rmtree(path, ignore_errors=True)
        print("{} dropped".format(path))

def createDir(path)       :
    if os.path.isdir(path):
        dropDir(path)

    os.makedirs(path)
    print("{} created".format(path))

def deleteFiles(listFile):
    for file in listFile:
        if os.path.isfile(file) :
            os.remove(file)
            print("{} deleted".format(file))


##### Delete

def dropWebSourceDir():
    dropDir(os.path.join(pathDataLake, webSource))
    
def dropCuratedDir():
    dropDir(os.path.join(pathDataLake, curated))
    
def dropLandingDir():
    dropDir(os.path.join(pathDataLake, landing))

def dropErrorDir():
    dropDir(os.path.join(pathDataLake, error))

def dropMetaDir():
    dropDir(os.path.join(pathDataLake, metaDir))

def dropMetaFiles(listMetaName):
    filesToDelete=[]
    for mFile in listMetaName:
        if mFile in metaFilesDict.keys() :
            filesToDelete.append(os.path.join(pathDataLake, metaDir, metaFilesDict[mFile]))

    deleteFiles(filesToDelete)

def truncateRefined():
    mydbConnection = None
    cursor = None
    try:
        print("TRUNCATE REFINED")
        tables={"dim_entreprise",
                "dim_localisation",
                "dim_type",
                "fact_emp",
                "fact_score",
                "dim_calendar",
                "dim_fonctions",
                "dim_fonction_emp",
                "dim_secteurs",
                "dim_secteur_emp"}
        for table in tables:
            sql = "TRUNCATE `{}`;".format(table)
            mydbConnection = connector.connect(
                host=serveur,
                user=user,
                passwd=password,
                database=bdd
            )
            cursor = mydbConnection.cursor()
            cursor.execute(sql)
            mydbConnection.commit()
            cursor.close()
            print("TRUNCATE {}".format(table))

    except connector.Error as error:
        print("-- Problème acces à la BdD : {}".format(error))
    finally :
        if mydbConnection.is_connected():

            mydbConnection.close()




###### CREATE

def createWebSourceDir():
    dropDir(os.path.join(pathDataLake, webSource))

def createCuratedDir():
    dropDir(os.path.join(pathDataLake, curated))


def createLandingDir():
    dropDir(os.path.join(pathDataLake, landing))


def createErrorDir():
    dropDir(os.path.join(pathDataLake, error))


def createMetaDir():
    dropDir(os.path.join(pathDataLake, metaDir))



def copyOrigineToSource(param):

    if param == None:
        raise ValueError("Pas de liste de fichier à déplacer!")
    if not isinstance(param, list):
        raise ValueError("Liste de fichier à déplacer invalide : {}!".format(type(param)))
    if pathDataLake == None:
        raise FileNotFoundError("Dossier Datalake non définit")
    if not os.path.isdir(pathDataLake):
        raise FileNotFoundError("Dossier datalake : {}".format(os.path.join(pathDataLake, webSource)))
    if pathOrigine == None:
        raise FileNotFoundError("Dossier Origine non définit")
    if not os.path.isdir(os.path.join(pathOrigine, webSource)):
        raise FileNotFoundError("Dossier Origine introuvable : {} ".format(os.path.join(pathOrigine, webSource)))

    for f in param :
        #print("FROM {}".format(os.path.join(pathOrigine, webSource,f)))
        #print("TO   {}".format(os.path.join(pathDataLake, webSource,f)))
        try:
            shutil.copy2(os.path.join(pathOrigine, webSource,f),os.path.join(pathDataLake, webSource,f))
            print(f)
        except shutil.SameFileError as err:
            print("Fichier déjà présent "+err)




####### Traitements
def usage(exitCode):
    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print( '      ex: {} [-h] [-d "path"] [-o "path"][-s serveur] [-b base] [-u user] [-p password] '.format(SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")

    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n")
    print(" -o | --origine : \n"
          "              definition du dossier d'origine des données")
    print(" -s | --serveur : \n"
          "              hote du serveur mysql")
    print(" -b | --base : \n"
          "              nom de la base de données")
    print(" -u | --user : \n"
          "              nom de l'utilisateur de la base de donnée")
    print(" -p | --password : \n"
          "              mot de passe de l'utilisateur")
    print("*" * 80)
    sys.exit(exitCode)

##################
def setUser(arg):

    global user
    if arg != "":
        user = arg


def setPassword(arg):

    global password
    if arg != "":
        password = arg


def setServeur(arg):

    global serveur
    if arg != "":
        serveur = arg


def setDataBase(arg):

    global bdd
    if arg != "":
        bdd = arg


def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param appli: nom de l'application
    :return:
    """

    try:
        opts, args = getopt.getopt(argv, "hd:o:u:p:s:b:", ["help", "datalake=","origine=","user=","password=","serveur=","base="])
    except getopt.GetoptError:
        usage(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-d", "--datalake"):
            setDatalake(arg)
        elif opt in ("-o", "--origine"):
            setOrigine(arg)
        elif opt in ("-u", "--user"):
            setUser(arg)
        elif opt in ("-p", "--password"):
            setPassword(arg)
        elif opt in ("-s", "--serveur"):
            setServeur(arg)
        elif opt in ("-b", "--base"):
            setDataBase(arg)
        else:
            usage(1)


# --------------------------------------
# programme Principal
# --------------------------------------

def setDatalake(path):
    #print("Définition dossier Datalake {} ".format(path))
    global pathDataLake
    if os.path.exists(path) and ( not os.path.isfile(path)):
        pathDataLake = path
        print("Dossier Datalake définit à {} ".format(pathDataLake))
    else:
        raise FileNotFoundError("Chemin datalake des datas non valide : {}".format(path))


def setOrigine(path):
    #print("Définition dossier Dépôt Origine {} ".format(path))
    global pathOrigine
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathOrigine = path
        print("Dossier Dépôt Origine définit  {} ".format(pathOrigine))
    else:
        raise FileNotFoundError("Chemin origine des datas non valide : {}".format(path))



#################################




def removeLanding():
    print("-" * 100)
    print("{}".format(sys._getframe().f_code.co_name))
    if not os.path.isdir(pathDataLake):
        raise FileNotFoundError("Chemin datalake des datas non valide : {}".format(pathDataLake))
    dropLandingDir()
    dropCuratedDir()
    dropMetaFiles(['tec','cur','ref'])
    truncateRefined()
    print("-" * 100)

def Quit():
    print("*" * 80)
    print(" FIN ")
    print("*" * 80)
    sys.exit(0)

def reinit():
    print("-" * 100)
    print("{}".format(sys._getframe().f_code.co_name))
    createWebSourceDir()
    if not os.path.isdir(pathDataLake):
        raise FileNotFoundError("Chemin datalake des datas non valide : {}".format(pathDataLake))
    dropLandingDir()
    dropCuratedDir()
    dropMetaFiles(['tec', 'cur', 'ref'])
    dropMetaDir()
    dropErrorDir()
    truncateRefined()
    print("-" * 100)


def loadFiles():
    print("-" * 100)
    print("{}".format(sys._getframe().f_code.co_name))
    if pathOrigine == None:
        raise FileNotFoundError("Dossier Origine non définit")
    if not os.path.isdir(os.path.join(pathOrigine, webSource)):
        raise FileNotFoundError("Dossier Origine introuvable : {} ".format(os.path.join(pathOrigine, webSource)))

    listFiles = os.listdir(os.path.join(pathOrigine, webSource))
    print("Nombre de fichier disponnible : {}".format(len(listFiles)))
    min = 0
    max = 10
    min = int(input("indice minimum [{}] : ".format(min)) or min)
    max = int(input("indice maximum [{}] : ".format(max)) or max)

    if min > max :
        raise ValueError("Minimum supérieur à maximum")

    print("De {} à {} ".format(min,max))
    print("{}".format(listFiles[min:max]))
    copyOrigineToSource(listFiles[min:max])
    print("Copie terminé de {} à {} ".format(min,max))
    print("-" * 100)

def loadOneFile():
    print("-" * 100)
    print("{}".format(sys._getframe().f_code.co_name))
    if pathOrigine == None:
        raise FileNotFoundError("Dossier Origine non définit")
    if not os.path.isdir(os.path.join(pathOrigine, webSource)):
        raise FileNotFoundError("Dossier Origine introuvable : {} ".format(os.path.join(pathOrigine, webSource)))


    file = str(input("nom du fichier  : "))


    if file == None :
        raise ValueError("Nom de fichier invalide")
    if file.strip(" ") ==  "" :
        raise ValueError("Nom de fichier invalide")


    print("{}".format(file))
    copyOrigineToSource([file])
    print("Copie terminé de {} ".format(file))
    print("-" * 100)

def removeRefined():
    print("-" * 100)
    print("{}".format(sys._getframe().f_code.co_name))
    dropMetaFiles(['ref'])
    truncateRefined()
    print("-" * 100)

def reinitForN():
    reinit()
    min=0
    max = 10
    max = int(input("combien de fichier [{}] : ".format(max)) or max)
    listFiles = os.listdir(os.path.join(pathOrigine, webSource))
    print("De {} à {} ".format(min, max))
    copyOrigineToSource(listFiles[min:max])

def traitementMenu(indice=0):
    menu = {
        0: Quit,
        1: removeLanding,
        2: reinit,
        3: loadFiles,
        4: removeRefined,
        5: reinitForN,
        6:loadOneFile
    }
    func = menu.get(indice, lambda: 'Invalid')

    return func()


if __name__ == "__main__":
    try:

        # Recupération des arguments
        parseArg(sys.argv[1:])
        while(True):
            menu = {
                0: 'Quit',
                1: 'Remove Landing',
                2: 'Remove All',
                3: 'Load File',
                4: 'Remove Refined',
                5: 'Remove ALL et Load File',
                6: 'Charger un fichier particulier'
            }
            # affichage menu
            print("*" * 80)
            print("datalake : {}".format(pathDataLake))
            print("origine  : {}".format(pathOrigine))
            print("*" * 80)
            for key, label in menu.items():
                print("{} : {}".format(key, label))

            choice = int(input("Choix : "))
            traitementMenu(choice)
    except ValueError as err:
        print("ERREUR Type {} : {}".format(type(err),err))
        usage(1)
    except FileNotFoundError as err:
        print("ERREUR Type {} : {}".format(type(err), err))
        usage(1)
