-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 03 mai 2020 à 18:17
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `donnemassive`
--

-- --------------------------------------------------------

--
-- Structure de la table `dim_calendar`
--

CREATE TABLE `dim_calendar` (
  `id_date` varchar(10) NOT NULL,
  `calendar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_entreprise`
--

CREATE TABLE `dim_entreprise` (
  `id_entreprise` varchar(11) NOT NULL,
  `nom_entreprise` varchar(50) NOT NULL,
  `SiegeSocial` varchar(50) NOT NULL,
  `pays` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_fonctions`
--

CREATE TABLE `dim_fonctions` (
  `id_fonction` varchar(50) NOT NULL,
  `fonction` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_fonction_emp`
--

CREATE TABLE `dim_fonction_emp` (
  `id_fonction` varchar(50) NOT NULL,
  `id_employe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_localisation`
--

CREATE TABLE `dim_localisation` (
  `id_localisation` varchar(50) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `pays` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_secteurs`
--

CREATE TABLE `dim_secteurs` (
  `id_secteur` varchar(50) NOT NULL,
  `secteur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_secteur_emp`
--

CREATE TABLE `dim_secteur_emp` (
  `id_secteur` varchar(50) NOT NULL,
  `id_employe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_type`
--

CREATE TABLE `dim_type` (
  `id_type` varchar(50) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_emp`
--

CREATE TABLE `fact_emp` (
  `id_employe` varchar(50) NOT NULL,
  `id_type` varchar(50) NOT NULL,
  `id_entreprise` varchar(50) NOT NULL,
  `id_localisation` varchar(50) NOT NULL,
  `intitule` varchar(50) NOT NULL,
  `commentaire` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_score`
--

CREATE TABLE `fact_score` (
  `id_avis` varchar(11) NOT NULL,
  `id_entreprise` varchar(11) NOT NULL,
  `id_date` varchar(10) NOT NULL,
  `score` int(11) NOT NULL,
  `avis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dim_calendar`
--
ALTER TABLE `dim_calendar`
  ADD PRIMARY KEY (`id_date`),
  ADD UNIQUE KEY `calendar` (`calendar`);

--
-- Index pour la table `dim_entreprise`
--
ALTER TABLE `dim_entreprise`
  ADD PRIMARY KEY (`id_entreprise`),
  ADD UNIQUE KEY `nom_entreprise` (`id_entreprise`,`nom_entreprise`,`SiegeSocial`,`pays`) USING BTREE;

--
-- Index pour la table `dim_fonctions`
--
ALTER TABLE `dim_fonctions`
  ADD PRIMARY KEY (`id_fonction`),
  ADD UNIQUE KEY `fonction` (`fonction`);

--
-- Index pour la table `dim_fonction_emp`
--
ALTER TABLE `dim_fonction_emp`
  ADD PRIMARY KEY (`id_fonction`,`id_employe`);

--
-- Index pour la table `dim_localisation`
--
ALTER TABLE `dim_localisation`
  ADD PRIMARY KEY (`id_localisation`),
  ADD UNIQUE KEY `ville` (`ville`,`pays`);

--
-- Index pour la table `dim_secteurs`
--
ALTER TABLE `dim_secteurs`
  ADD PRIMARY KEY (`id_secteur`),
  ADD UNIQUE KEY `secteur` (`secteur`);

--
-- Index pour la table `dim_secteur_emp`
--
ALTER TABLE `dim_secteur_emp`
  ADD PRIMARY KEY (`id_secteur`,`id_employe`) USING BTREE;

--
-- Index pour la table `dim_type`
--
ALTER TABLE `dim_type`
  ADD PRIMARY KEY (`id_type`),
  ADD UNIQUE KEY `nom` (`nom`);

--
-- Index pour la table `fact_emp`
--
ALTER TABLE `fact_emp`
  ADD PRIMARY KEY (`id_employe`);

--
-- Index pour la table `fact_score`
--
ALTER TABLE `fact_score`
  ADD PRIMARY KEY (`id_avis`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
