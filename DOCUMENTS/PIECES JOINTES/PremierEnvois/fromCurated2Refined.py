# - * - coding: utf - 8 -
#
# chargement vers la refined

import csv

import fnmatch
import getopt
import hashlib
import logging
import os
import sys
import time
import uuid
from datetime import datetime
import pandas as pd

# Properties
pathDataLake = "../../DATALAKE/"
pathCurated = pathDataLake + "2_CURATED_ZONE"
curatedNameSuffix = "_curatedDb.csv"
pathLanding = pathDataLake + "1_LANDING_ZONE"
pathMetadata = pathDataLake + "METADATA"
metaDataTechName = "metaDataTech.csv"
metaDataCurName = "metaDataCurated.csv"
pathFileError = pathDataLake + "ERRORFILE"
bdd="donneemassive"
serveur="localhost"
user="user"
password="password"



SCRIPT_NAME = os.path.basename(sys.argv[0])

# --------------------------------------
# Definition logger application
# --------------------------------------
logging.basicConfig(level=logging.DEBUG,format="%(asctime)s : [ %(levelname)s ] %(message)s",datefmt='%m/%d/%Y %H:%M:%S')

logger = logging.getLogger(SCRIPT_NAME)

# --------------------------------------
# definition des methodes et fonctions
# --------------------------------------
def usage(exitCode):
    """
    afficher le parametre pris en compte par l'application
    :param exitCode: code de sortie après le usage
    """
    logger.debug("usage : {} ")

    print()
    print("*" * 80)
    print("USAGE {}".format(SCRIPT_NAME))
    print("*" * 80)
    print( '      ex: {} [-h] [-d "path"] [-t seconde]'.format(SCRIPT_NAME))
    print("\n\n Arguments : \n"
          " -h | --help : \n"
          "              Affichage les règles d'usage que celui par défaut.")
    print(" -t | --timer : \n"
          "              cadence de scanning.")
    print(" -d | --datalake : \n"
          "              definition d'un dossier datalake unique avec les chemins par defaut.\n"
          "               /!\\ réinitialisation des chemins par défaut pour les landing et sourceweb")
    print("*" * 80)
    sys.exit(exitCode)

#################
def setCurated(path):
    """
    initialisation de webSourcePath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    logger.debug("Definition chemin curated {}".format(path))
    global pathCurated
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathCurated = path
    else:
        logger.info("Creation du dossier de curatedZone [{}] ".format(path))
        try :
            os.makedirs(path)
            pathCurated = path
        except :
            raise IOError("Impossible de construire {}".format(path))
##################
def setLanding(path):
    """
    initialisation de LandingPath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathLanding
    logger.debug("Definition chemin landing")
    if os.path.exists(path) and (not os.path.isfile(path)):
        pathLanding = path
    else:
        logger.info("Creation du dossier de landing [{}] ".format(path))
        try :
            os.makedirs(path)
            pathLanding = path
        except :
            raise IOError("Impossible de construire {}".format(path))

##################
def setTimer(value):
    """
      initialisation du timer de scan
      :param value: valeur du timer
      :raise ValueError si timer  inférieur ou égal à 0
      """
    global timer

    logger.debug("Definition timer de scanning")
    if int(value) >= 0:
        timer = int(value)
    else:
        raise ValueError("Valeur pour le timer invalide ")

#################
def setMetadata(path):
    """
    initialisation de metapath
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathMetadata
    logger.debug("Definition chemin meta")
    if os.path.exists(path) and ( not os.path.isfile(path)):
        pathMetadata = path
    else:
        logger.info("Creation du dossier de meta [{}] ".format(path))
        try :
            os.makedirs(path)
            pathMetadata = path
        except :
            raise IOError("Impossible de construire {}".format(path))

#################
def setErrorFile(path):
    """
    initialisation de errorFile
    :param path: valeur de path
    :raise ValueError si chemin non valide
    """
    global pathFileError
    logger.debug("Definition chemin errorFile")
    if os.path.exists(path) and ( not os.path.isfile(path)):
        pathFileError = path
    else:
        logger.info("Creation du dossier de errorFile [{}] ".format(path))
        try :
            os.makedirs(path)
            pathFileError = path
        except :
            raise IOError("Impossible de construire {}".format(path))

##################
def setDatalake(path):
    """
         initialisation du path datalake
         :param value: chemin du datalake
         :raise ValueError si path invalide
    """
    global pathDataLake

    logger.debug("Definition chemin datalake [ {} ]".format(path))
    if os.path.exists(path) and ( not os.path.isfile(path)):
        pathDataLake = path
        setCurated(pathDataLake + "\\2_CURATED_ZONE")
        setLanding(pathDataLake + "\\1_LANDING_ZONE")
        setMetadata(pathDataLake + "\\METADATA")
        setErrorFile(pathDataLake + "\\ERRORFILE")
    else:
        raise ValueError("Chemin datalake des datas non valide : {}".format(path))

##################
def setUser(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name.upper(),arg))
    global user
    if arg != "":
        user = arg


def setPassword(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name.upper(), arg))
    global password
    if arg != "":
        password = arg


def setServeur(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name.upper(), arg))
    global serveur
    if arg != "":
        serveur = arg


def setDataBase(arg):
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name.upper(), arg))
    global bdd 
    if arg != "":
        bdd = arg


def parseArg(argv):
    """
    Methode principale
    pars des argument pour traiter les données

    :param argv: arguments
    :param appli: nom de l'application
    :return:
    """
    logger.debug("ENTER IN {} args = [{}]".format(sys._getframe().f_code.co_name.upper(), argv))
    try:
        opts, args = getopt.getopt(argv, "hd:t:u:p:s:b:", ["help", "datalake=","timer=","user=","password=","serveur=","base="])
    except getopt.GetoptError:
        usage(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-f", "--filter"):
            setCurated(arg)
        elif opt in ("-t", "--timer"):
            setTimer(arg)
        elif opt in ("-d", "--datalake"):
            logger.debug(arg)
            setDatalake(arg)
        elif opt in ("-u", "--user"):
            setUser(arg)
        elif opt in ("-p", "--password"):
            setPassword(arg)
        elif opt in ("-s", "--serveur"):
            setServeur(arg)
        elif opt in ("-b", "--base"):
            setDataBase(arg)
        else:
            usage(1)


# --------------------------------------
# programme Principal
# --------------------------------------

def traitement():
    logger.debug("ENTER IN {}".format(sys._getframe().f_code.co_name.upper()))
    raise NotImplementedError("! NON IMPLEMENTE ! ")
    connecteToDb()


if __name__ == "__main__":
    print("*" * 80)
    print("PROGRAMME SOURCE WEB TO LANDING")
    print("*" * 80)
    # Recupération des arguments
    parseArg(sys.argv[1:])
    traitement()