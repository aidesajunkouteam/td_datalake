-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 28 avr. 2020 à 21:33
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `donnemassive`
--

-- --------------------------------------------------------

--
-- Structure de la table `dim_entreprise`
--

DROP TABLE IF EXISTS `dim_entreprise`;
CREATE TABLE `dim_entreprise` (
  `id_entreprise` varchar(11) NOT NULL,
  `nom_entreprise` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_localisation`
--

DROP TABLE IF EXISTS `dim_localisation`;
CREATE TABLE `dim_localisation` (
  `id_localisation` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_type`
--

DROP TABLE IF EXISTS `dim_type`;
CREATE TABLE `dim_type` (
  `id_type` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_emp`
--

DROP TABLE IF EXISTS `fact_emp`;
CREATE TABLE `fact_emp` (
  `id_employe` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_localisation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_score`
--

DROP TABLE IF EXISTS `fact_score`;
CREATE TABLE `fact_score` (
  `id_avis` varchar(11) NOT NULL,
  `id_entreprise` varchar(11) NOT NULL,
  `score` int(11) NOT NULL,
  `avis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dim_entreprise`
--
ALTER TABLE `dim_entreprise`
  ADD PRIMARY KEY (`id_entreprise`),
  ADD UNIQUE KEY `nom_entreprise` (`nom_entreprise`);

--
-- Index pour la table `dim_type`
--
ALTER TABLE `dim_type`
  ADD PRIMARY KEY (`id_type`);

--
-- Index pour la table `fact_emp`
--
ALTER TABLE `fact_emp`
  ADD PRIMARY KEY (`id_employe`);

--
-- Index pour la table `fact_score`
--
ALTER TABLE `fact_score`
  ADD PRIMARY KEY (`id_avis`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dim_type`
--
ALTER TABLE `dim_type`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fact_emp`
--
ALTER TABLE `fact_emp`
  MODIFY `id_employe` int(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
