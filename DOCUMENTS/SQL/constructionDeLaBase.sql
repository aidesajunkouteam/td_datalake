-- --------------------------------------------------------

--
-- Structure de la table `dim_entreprise`
--

CREATE TABLE `dim_entreprise` (
  `id_entreprise` int(11) NOT NULL,
  `nom_entreprise` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_localisation`
--

CREATE TABLE `dim_localisation` (
  `id_localisation` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `dim_type`
--

CREATE TABLE `dim_type` (
  `id_type` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_emp`
--

CREATE TABLE `fact_emp` (
  `id_employe` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `id_localisation` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `fact_score`
--

CREATE TABLE `fact_score` (
  `id_score` int(11) NOT NULL,
  `id_entreprise` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `avis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dim_entreprise`
--
ALTER TABLE `dim_entreprise`
  ADD PRIMARY KEY (`id_entreprise`);

--
-- Index pour la table `dim_type`
--
ALTER TABLE `dim_type`
  ADD PRIMARY KEY (`id_type`);

--
-- Index pour la table `fact_emp`
--
ALTER TABLE `fact_emp`
  ADD PRIMARY KEY (`id_employe`);

--
-- Index pour la table `fact_score`
--
ALTER TABLE `fact_score`
  ADD PRIMARY KEY (`id_score`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dim_entreprise`
--
ALTER TABLE `dim_entreprise`
  MODIFY `id_entreprise` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `dim_type`
--
ALTER TABLE `dim_type`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fact_emp`
--
ALTER TABLE `fact_emp`
  MODIFY `id_employe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `fact_score`
--
ALTER TABLE `fact_score`
  MODIFY `id_score` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;